#! /usr/bin/perl -w
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


@BOXES=qw / Top Bottom ASide CSide /;
$classID = 9203;
@layerid = qw /X1 U V X2 /;

print "now loop over Stations\n";
for $i (1 ... 3 ) {
    print "  station $i\n";
    foreach $BOX ( @BOXES ) {
	print "  station $i\n";


            if ($BOX eq "CSide" || $BOX eq "ASide"){
                $Append = "_1";
            }
            else {
                $Append = "";
            }

	$FILENAME=">ITT${i}Station${BOX}Structure.xml";
	
#now open the output file:
	open(OUT,$FILENAME);
#write into the output file:
	print OUT qq/<?xml version="1.0" encoding="ISO-8859-1"?>\n/;
	print OUT qq/<!DOCTYPE DDDB SYSTEM "..\/..\/..\/DTD\/structure.dtd">\n/;
	print OUT "<DDDB>\n";
	print OUT "<!-- ***************************************************************** -->\n";
	print OUT "<!-- *            Description of the ITT${i} ${BOX} structure             * -->\n";
	print OUT "<!-- *                                                               * -->\n";
	print OUT "<!-- *                         Author: K. Vervink                    * -->\n";
	print OUT "<!-- *                          Date: 11-08-2005                     * -->\n";
	print OUT "<!-- ***************************************************************** -->\n";
	print OUT "\n";
	print OUT qq/  <detelem classID="$classID" name="${BOX}Box" \>\n/;
	print OUT "    <author>K. Vervink</author>\n";
	print OUT "    <version>1.0</version>\n";


	if (($i eq "2" && $BOX eq "Bottom") || ($i eq "3" && $BOX eq "Top")){
	    if ($i eq "2"){
            $Orient = "Center";
		print OUT qq/    <geometryinfo lvname    = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/lvIT_410_V_4to7_Full${Orient}Box\" \n/;
	        print OUT qq/                  condition = \"\/dd\/Conditions\/Alignment\/IT\/ITT${i}${BOX}Box\"\n/;
        	print OUT qq/                  support   = "\/dd\/Structure\/LHCb\/AfterMagnetRegion\/T\/IT\/Station${i}"\n/;
                print OUT qq/                  npath     = "pvIT_410_V_4to7_${BOX}Box"\/>\n/;
	    } 
	    if ($i eq "3"){
            $Orient = "Center";
		print OUT qq/    <geometryinfo lvname    = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/lvIT_410_V_1to4_X2_1to4_Full${Orient}Box\" \n/;
       	 	print OUT qq/                  condition = \"\/dd\/Conditions\/Alignment\/IT\/ITT${i}${BOX}Box\"\n/;
        	print OUT qq/                  support   = "\/dd\/Structure\/LHCb\/AfterMagnetRegion\/T\/IT\/Station${i}"\n/;
        	print OUT qq/                  npath     = "pvIT_410_V_1to4_X2_1to4_${BOX}Box"\/>\n/;
	    }

	} else {
	    if ($BOX eq "CSide" || $BOX eq "ASide"){
		$Orient = "Side";	    
		print OUT qq/        <geometryinfo lvname    = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/lvITFull${Orient}Box\"\n/;
        	print OUT qq/                      condition = \"\/dd\/Conditions\/Alignment\/IT\/ITT${i}${BOX}Box\"\n/;
        	print OUT qq/                      support   = "\/dd\/Structure\/LHCb\/AfterMagnetRegion\/T\/IT\/Station${i}"\n/;
		print OUT qq/                      npath     = "pvIT${BOX}Box"\/>\n/;
		$j = "S";  
	    }else{
		$Orient = "Center";
		print OUT qq/        <geometryinfo lvname    = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/lvITFull${Orient}Box\" \n/;
        	print OUT qq/                      condition = \"\/dd\/Conditions\/Alignment\/IT\/ITT${i}${BOX}Box\"\n/;
	        print OUT qq/                      support   = "\/dd\/Structure\/LHCb\/AfterMagnetRegion\/T\/IT\/Station${i}"\n/;
		print OUT qq/                      npath     = "pvIT${BOX}Box"\/>\n/;
		$j = "C";
	    }
	}

        if ($BOX eq "ASide") {$BoxNumber ="2";}
        if ($BOX eq "CSide")  {$BoxNumber ="1";}   
        if ($BOX eq "Top")  {$BoxNumber ="4";}   
        if ($BOX eq "Bottom") {$BoxNumber ="3";} 

	print OUT qq/    <param name = \"boxID\" type = \"int\"> ${BoxNumber} <\/param> \n/;

	print OUT "\n"; 

	foreach $layer (@layerid){
	    #print OUT qq/    <detelemref href = "Layer\/ITT${i}${BOX}BoxLayer${layer}Structure\.xml\#Layer${layer}"\/>\n/;
            print OUT qq/    <detelemref href = "Layer\/${BOX}${layer}\@ITT${i}Structure${Append}\.xml\#Layer${layer}"\/>\n/;
#	    $classID++;
	}	
	print OUT "\n";
	print OUT "  </detelem>\n";
	print OUT "</DDDB>\n";	
	close(OUT);
	
    }
    
}


#! /usr/bin/perl -w
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


@BOXES=qw / Top Bottom ASide CSide /;
$classID = 9204;
@LAYERID = qw /X1 U V X2 /;
@LADDERID = qw /1 2 3 4 5 6 7 /;

print "now loop over Stations\n";
for $i (1 ... 3 ) {
    print "  station $i\n";
    foreach $BOX(@BOXES){
	print "     box $BOX\n";
        foreach $LAYER(@LAYERID){

            if ($BOX eq "CSide" || $BOX eq "ASide"){
            	$Append = "_1";
            }
            else {
                $Append = "";
            }

	    #$FILENAME=">ITT${i}${BOX}BoxLayer${LAYER}Structure.xml";
            $FILENAME=">${BOX}${LAYER}\@ITT${i}Structure${Append}.xml";
	    
#now open the output file:
	    open(OUT,$FILENAME);
#write into the output file:
	    print OUT qq/<?xml version="1.0" encoding="ISO-8859-1"?>\n/;
	    print OUT qq/<!DOCTYPE DDDB SYSTEM "..\/..\/..\/..\/DTD\/structure.dtd">\n/;
	    print OUT "<DDDB>\n";
	    print OUT "<!-- ***************************************************************** -->\n";
	    print OUT "<!-- *            Description of the ITT${i} ${BOX} ${LAYER}-Layer Structure   * -->\n";
	    print OUT "<!-- *                                                               * -->\n";
	    print OUT "<!-- *                        Author: K. Vervink                     * -->\n";
	    print OUT "<!-- *                          Date: 11-08-2005                     * -->\n";
	    print OUT "<!-- ***************************************************************** -->\n";
	    print OUT "\n";
	    print OUT qq/  <detelem classID="$classID" name="Layer${LAYER}">\n/;
	    print OUT "    <author>K. Vervink</author>\n";
	    print OUT "    <version>1.0</version>\n";
	    if ($BOX eq "CSide" || $BOX eq "ASide"){
		$Length = "Long";
		$Append = "_1";
	    }
	    else {
		$Length = "Short";
		$Append = "";
	    }
	    if ($LAYER eq "X1") {
		$LayerOrient ="0.0*degree";
		$LayerNumber = "1";
	    }
	    if ($LAYER eq "U")  {
		$LayerOrient = "-5.0*degree";
		$LayerNumber = "2";
	    }   
	    if ($LAYER eq "V")  {
		$LayerOrient ="5.0*degree";
		$LayerNumber = "3";
	    }   
	    if ($LAYER eq "X2") {
		$LayerOrient ="0.0*degree";
		$LayerNumber = "4";
	    } 
	    if ($BOX eq "CSide") {
                if ($LAYER eq "X1") {
                    $LOGLAYER = "X2";
                }
                if ($LAYER eq "U") {
                    $LOGLAYER = "V";
                }
                if ($LAYER eq "V") {
                    $LOGLAYER = "U";
                }
                if ($LAYER eq "X2") {
                    $LOGLAYER = "X1";
                }
            } else {
                $LOGLAYER = $LAYER;
            }
	    
	    print OUT qq/    <geometryinfo lvname    = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/lvIT${LOGLAYER}${Length}Layer\" \n/;
            print OUT qq/                  condition = \"\/dd\/Conditions\/Alignment\/IT\/ITT${i}${BOX}Layer${LAYER}\"\n/;
	    print OUT qq/                  support   = "\/dd\/Structure\/LHCb\/AfterMagnetRegion\/T\/IT\/Station${i}\/${BOX}Box"\n/;
	    print OUT qq/                  npath     = "pvIT${LOGLAYER}${Length}Layer"\/>\n/;
	    print OUT "\n";
	    print OUT qq/     <param name = \"layerID\" type = \"int\"> ${LayerNumber} <\/param> \n/;
	    print OUT qq/     <param name = \"stereoangle\" type = \"double\"> ${LayerOrient} <\/param> \n/;
	    print OUT "\n";
#	    print OUT "<!-- \n";
	    foreach $LADDER (@LADDERID){
		#print OUT qq/     <detelemref href = "Ladders\/ITT${i}${BOX}Layer${LAYER}Ladder${LADDER}Structure\.xml\#Ladder${LADDER}"\/>\n/;
                #print OUT qq/     <detelemref href = "Ladders\/${BOX}${LAYER}L${LADDER}\@ITT${i}Structure${Append}\.xml\#Ladder${LADDER}"\/>\n/;
                print OUT qq/     <detelemref href = "Ladders\/${BOX}${LAYER}L${LADDER}\@ITT${i}Structure\.xml\#Ladder${LADDER}"\/>\n/;
#		$classID++;
	    }	
#	    print OUT " -->\n";
	    print OUT "\n";
	    print OUT "  </detelem>\n";
	    print OUT "</DDDB>\n";
	    
	    close(OUT);
	    
	}
	
    }
    
}

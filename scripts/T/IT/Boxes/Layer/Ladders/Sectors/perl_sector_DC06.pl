#! /usr/bin/perl -w
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


@BOXES=qw / Top Bottom ASide CSide /;
$classID = 9206;
$laddernr = 0; 
$ladderDC = 0;
@LAYERID = qw /X1 U V X2 /;
@LADDERID = qw /1 2 3 4 5 6 7 /;
@SECTORID = qw /1/; 


print "now loop over Stations\n";
for $i (1 ... 3 ) {
    print "  station $i\n";
    foreach $BOX(@BOXES){
        print "     box $BOX\n";
        foreach $LAYER(@LAYERID){
            print "        layer $LAYER\n";
            foreach $LADDER(@LADDERID){
                if ($BOX eq "Top"){
                $Append = "";
                    if ($LAYER eq "V"){
                        $laddernr = $LADDER;
                    } elsif ($LAYER eq "U" || $LAYER eq "X2" || $LAYER eq "X1"){
                        $laddernr = 8 - $LADDER;
                    }
		    $Length = "Short";
                    $nSensor = 1;
                    $CAP = "18";
                    $SeK = "320Sector";
                    $KeS = "Sector320";
                    $SenS = "320Sensor";
                    $GoodLayer = $LAYER;
                } elsif ($BOX eq "Bottom"){
                $Append = "";
                    if ($LAYER eq "V"){
                        $laddernr = 8 - $LADDER;
                    } elsif ($LAYER eq "U" || $LAYER eq "X2" || $LAYER eq "X1"){
                        $laddernr = $LADDER;
                    }
                    $Length = "Short";
                    $nSensor = 1;
                    $CAP = "18";
                    $SeK = "320Sector";
                    $KeS = "Sector320";
                    $SenS = "320Sensor";
                    $GoodLayer = $LAYER;
                # Beware of the Y axis rotation of the CSide Box --> revert order of X1<-->X2 and U<-->V
                } elsif ($BOX eq "CSide"){
                $Append = "_1";
                    if ($LAYER eq "U"){
                        $laddernr = $LADDER;
                        if ($LAYER eq "U"){
                            $GoodLayer = "V";
                        } 
                    } elsif ($LAYER eq "V" || $LAYER eq "X2" || $LAYER eq "X1"){
                        $laddernr = 8 - $LADDER;
                        if ($LAYER eq "X2"){
                            $GoodLayer = "X1";
                        } elsif ($LAYER eq "U"){
                            $GoodLayer = "V";
                        } elsif ($LAYER eq "X1"){
			    $GoodLayer = "X2";
			}
                    }
                    $Length = "Long";
                    $nSensor = 2;
                    $SeK = "410Sector";
                    $KeS = "Sector410";
                    $SenS = "410Sensor";
                    $CAP = "33"
                } elsif ($BOX eq "ASide"){
                $Append = "_1";
                    if ($LAYER eq "V"){
                        $laddernr = 8 - $LADDER;
                    } elsif ($LAYER eq "U" || $LAYER eq "X2" || $LAYER eq "X1"){
                        $laddernr = $LADDER;
                    }
                    $Length = "Long";
                    $nSensor = 2;
                    $CAP = "33";
                    $SeK = "410Sector";
                    $KeS = "Sector410";
                    $SenS = "410Sensor";
                    $GoodLayer = $LAYER;
                }



		#$FILENAME=">ITT${i}${BOX}Layer${LAYER}Ladder${laddernr}Structure.xml";
                #$FILENAME=">${BOX}${LAYER}L${LADDER}_$SeK\@ITT${i}Structure${Append}.xml";
                $FILENAME=">${BOX}${LAYER}L${LADDER}_$SeK\@ITT${i}Structure.xml";
		#now open the output file:
		open(OUT,$FILENAME);
		#write into the output file:
		print OUT qq/<?xml version="1.0" encoding="ISO-8859-1"?>\n/;
		print OUT qq/<!DOCTYPE DDDB SYSTEM "..\/..\/..\/..\/..\/..\/DTD\/structure.dtd">\n/;
		print OUT "<DDDB>\n";
		print OUT "<!-- ***************************************************************** -->\n";
		print OUT "<!-- *   Description of the ITT${i} ${BOX} ${LAYER}-Layer ${LADDER}-Ladder $SeK-Sector Structure      * -->\n";
		print OUT "<!-- *                                                               * -->\n";
		print OUT "<!-- *                        Author: K. Vervink                     * -->\n";
		print OUT "<!-- *                          Date: 11-08-2005                     * -->\n";
		print OUT "<!-- ***************************************************************** -->\n";
		print OUT "\n";
		print OUT qq/  <detelem classID="$classID" name="_${SeK}">\n/;
		print OUT "    <author>V. Fave</author>\n";
		print OUT "    <version>1.0</version>\n";
		print OUT qq/    <geometryinfo lvname    = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/Ladder\/lv${Length}${SeK}\" \n/;
                print OUT qq/                  condition = \"\/dd\/Conditions\/Alignment\/IT\/ITT${i}${BOX}Layer${LAYER}Ladder${LADDER}_${SeK}\"\n/;
		print OUT " "x16 . qq/  support   = "\/dd\/Structure\/LHCb\/AfterMagnetRegion\/T\/IT\/Station${i}\/${BOX}Box\/Layer${LAYER}\/Ladder${LADDER}"\n/;
		print OUT qq/                  npath     = "pv${Length}${SeK}"\/>\n/;
		#print OUT qq/    <param name = \"sectorID\" type=\"int\"> $LADDER <\/param> \n/;
		print OUT "\n";
                print OUT qq/      <param name = \"version" type = "string"> DC06 <\/param> \n/;
                print OUT qq/      <param name = \"pitch\" type=\"double\"> 0.198*mm <\/param> \n/;
                print OUT qq/      <param name = \"numStrips\" type=\"int\"> 384 <\/param> \n/;
                print OUT qq/      <param name = \"verticalGuardRing\" type=\"double\"> 1.00*mm <\/param> \n/;
                print OUT qq/      <param name = \"bondGap\" type=\"double\"> 0.150*mm <\/param> \n/;
                print OUT qq/      <param name = \"capacitance\" type=\"double\"> $CAP*picofarad <\/param> \n/;
                print OUT qq/      <param name = \"type\" type=\"string\"> $Length <\/param> \n/;
                print OUT qq/      <param name = \"nSensors\" type=\"int\"> $nSensor <\/param> \n/;
                print OUT qq/      <conditioninfo name = \"Status\" condition= 
"\/dd\/Conditions\/ChannelInfo\/IT\/ReadoutSectors\/ITT${i}${BOX}Layer${LAYER}Sector${LADDER}"\ \/> \n/;
                print OUT "\n";

                if ( $Length eq "Short" ) {

		print OUT qq/    <detelem classID="9231" name="Sensor1">\n/;
		print OUT qq/      <geometryinfo lvname  = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/Ladder\/lv${Length}${SenS}\" \n/;
		print OUT " "x16 . qq/    support = "\/dd\/Structure\/LHCb\/AfterMagnetRegion\/T\/IT\/Station${i}\/${BOX}Box\/Layer${LAYER}\/Ladder${LADDER}\/_${SeK}"\n/;
		print OUT qq/                    npath   = "pvSensor320S"\n/;
                print OUT qq/                    condition = \"\/dd\/Conditions\/Alignment\/IT\/ITT${i}${BOX}Layer${LAYER}Ladder${LADDER}_${SeK}_Sensor1\"\ \/> \n/;
		print OUT qq/    <param name = \"sensorID\" type=\"int\"> 1 <\/param> \n/;
                
		print OUT "    </detelem> \n";

                } else {

                print OUT qq/    <detelem classID="9231" name="Sensor1">\n/;
                print OUT qq/      <geometryinfo lvname  = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/Ladder\/lv${Length}${SenS}\" \n/;
                print OUT " "x16 . qq/    support = "\/dd\/Structure\/LHCb\/AfterMagnetRegion\/T\/IT\/Station${i}\/${BOX}Box\/Layer${LAYER}\/Ladder${LADDER}\/_${SeK}"\n/;
                print OUT qq/                    npath   = "pvSensor410L1"\n/;
                print OUT qq/                    condition = \"\/dd\/Conditions\/Alignment\/IT\/ITT${i}${BOX}Layer${LAYER}Ladder${LADDER}_${SeK}_Sensor1\"\ \/> \n/;

		print OUT qq/    <param name = \"sensorID\" type=\"int\"> 1 <\/param> \n/;
                print OUT "    </detelem> \n";

                print OUT qq/    <detelem classID="9231" name="Sensor2">\n/;
                print OUT qq/      <geometryinfo lvname  = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/Ladder\/lv${Length}${SenS}\" \n/;
                print OUT " "x16 . qq/    support = "\/dd\/Structure\/LHCb\/AfterMagnetRegion\/T\/IT\/Station${i}\/${BOX}Box\/Layer$LAYER\/Ladder${LADDER}\/_${SeK}"\n/;
                print OUT qq/                    npath   = "pvSensor410L2"\n/;
                print OUT qq/                    condition = \"\/dd\/Conditions\/Alignment\/IT\/ITT${i}${BOX}Layer${LAYER}Ladder${LADDER}_${SeK}_Sensor2\"\ \/> \n/;
		print OUT qq/    <param name = \"sensorID\" type=\"int\"> 2 <\/param> \n/;
                print OUT "    </detelem> \n";


                }                

		print OUT "\n";
		print OUT "  </detelem>\n";
		print OUT "</DDDB>\n";
		close(OUT);
		
	    }
	    
	}
	
    }

}

#! /usr/bin/perl -w
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


@BOXES=qw / Top Bottom ASide CSide /;
$classID = 9205;
$laddernr = 0; 
@LAYERID = qw /X1 U V X2 /;
@LADDERID = qw /1 2 3 4 5 6 7 /;
@LADDER14 = qw /1 2 3 4 /;
@LADDER47 = qw /4 5 6 7 /;

print "now loop over Stations\n";
for $i (1 ... 3 ) {
    print "  station $i\n";
    foreach $BOX(@BOXES){
	print "     box $BOX\n";
        foreach $LAYER(@LAYERID){
	    print "        layer $LAYER\n";
	    foreach $LADDER(@LADDERID){
		if ($BOX eq "Top"){
                $Append = "";
		    if ($LAYER eq "X1" || $LAYER eq "V"){
		        $laddernr = $LADDER;
		    } elsif ($LAYER eq "U" || $LAYER eq "X2"){
		        $laddernr = 8 - $LADDER;
		    }
                    $Length = "Short";
                    $nSensor = 1;
                    $CAP = "18";
		    $GoodLayer = $LAYER;
		} elsif ($BOX eq "Bottom"){
                $Append = "";
                    if ($LAYER eq "X1" || $LAYER eq "V"){
                        $laddernr = 8 - $LADDER;
                    } elsif ($LAYER eq "U" || $LAYER eq "X2"){
                        $laddernr = $LADDER;
                    }
                    $Length = "Short";
                    $nSensor = 1;
                    $CAP = "18";
                    $GoodLayer = $LAYER;
		# Beware of the Y axis rotation of the CSide Box --> revert order of X1<-->X2 and U<-->V
		} elsif ($BOX eq "CSide"){
                $Append = "_1";
		    if ($LAYER eq "X2" || $LAYER eq "U"){
                        $laddernr = $LADDER;
			if ($LAYER eq "X1"){
			    $GoodLayer = "X2";
                        } elsif ($LAYER eq "V"){
			    $GoodLayer = "U";
			}
                    } elsif ($LAYER eq "V" || $LAYER eq "X1"){
                        $laddernr = 8 - $LADDER;
                        if ($LAYER eq "X2"){
                            $GoodLayer = "X1";
                        } elsif ($LAYER eq "U"){
                            $GoodLayer = "V";
                        }
                    }
		    $Length = "Long";
                    $nSensor = 2;
                    $CAP = "33"
		} elsif ($BOX eq "ASide"){
                $Append = "_1";
		    if ($LAYER eq "X1" || $LAYER eq "V"){
                        $laddernr = 8 - $LADDER;
                    } elsif ($LAYER eq "U" || $LAYER eq "X2"){
                        $laddernr = $LADDER;
                    }
		    $Length = "Long";
                    $nSensor = 2;
                    $CAP = "33";
                    $GoodLayer = $LAYER;
		}




		#$FILENAME=">ITT${i}${BOX}Layer${LAYER}Ladder${laddernr}Structure.xml";
                $FILENAME=">${BOX}${LAYER}L${LADDER}\@ITT${i}Structure${Append}.xml";
		#now open the output file:
		open(OUT,$FILENAME);
                #write into the output file:
		print OUT qq/<?xml version="1.0" encoding="ISO-8859-1"?>\n/;
		print OUT qq/<!DOCTYPE DDDB SYSTEM "..\/..\/..\/..\/..\/DTD\/structure.dtd">\n/;
		print OUT "<DDDB>\n";
		print OUT "<!-- ***************************************************************** -->\n";
		print OUT "<!-- * Description of the ITT${i} ${BOX} ${LAYER}-Layer ${LADDER}-Ladder Structure * -->\n";
		print OUT "<!-- *                                                               * -->\n";
		print OUT "<!-- *                        Author: K. Vervink                     * -->\n";
		print OUT "<!-- *                          Date: 11-08-2005                     * -->\n";
		print OUT "<!-- ***************************************************************** -->\n";
		print OUT "\n";
		print OUT qq/  <detelem classID="$classID" name="Ladder${LADDER}">\n/;
		print OUT "    <author>K. Vervink</author>\n";
		print OUT "    <version>1.0</version>\n";


	        if (($BOX eq "Top" && $i eq "3" && ($LAYER eq "V" || $LAYER eq "X2") && ($LADDER=~/[1-4]/)) || ($i eq "2" && $BOX eq "Bottom" && $LAYER eq "V" && ($LADDER=~/[4-7]/))){
              	    print OUT qq/      <geometryinfo lvname    = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/Ladder\/lv${Length}410Ladder\" \n/;
                    print OUT qq/                    condition = \"\/dd\/Conditions\/Alignment\/IT\/ITT${i}${BOX}Layer${LAYER}Ladder${LADDER}\"\n/;
        	    print OUT qq/      		     support   = \"\/dd\/Structure\/LHCb\/AfterMagnetRegion\/T\/IT\/Station${i}\/${BOX}Box\/Layer${LAYER}"\n/;
	            print OUT qq/      		     npath     = "pv${Length}Ladder${laddernr}"\/>\n/;
        	} else {
	            print OUT qq/      <geometryinfo lvname    = \"\/dd\/Geometry\/AfterMagnetRegion\/T\/IT\/Ladder\/lv${Length}Ladder\" \n/;
        	    print OUT qq/                    condition = \"\/dd\/Conditions\/Alignment\/IT\/ITT${i}${BOX}Layer${LAYER}Ladder${LADDER}\"\n/;
                    print OUT qq/                    support   = \"\/dd\/Structure\/LHCb\/AfterMagnetRegion\/T\/IT\/Station${i}\/${BOX}Box\/Layer${LAYER}"\n/;
		    print OUT qq/                    npath     = "pv${Length}Ladder${laddernr}"\/>\n/;
        	}

		#print OUT qq/    <param name = \"ladderID\" type=\"int\"> $laddernr <\/param> \n/;
                print OUT qq/    <param name = \"sectorID\" type=\"int\"> ${LADDER} <\/param> \n/;
		print OUT "\n";
                   print OUT qq/    <detelemref href = "Sectors\/${BOX}${LAYER}L${LADDER}Sector\@ITT${i}Structure_1\.xml\#Sector"\/>\n/;
                print OUT "\n";
		print OUT "  </detelem>\n";
		print OUT "</DDDB>\n";
		close(OUT);
		
	    }
	    
	}
	
    }

}

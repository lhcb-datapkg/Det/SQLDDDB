#!/bin/bash
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

export CORAL_DBLOOKUP_PATH=~/private/lb_conddb_dbs
export CORAL_AUTH_PATH=~/private/lb_conddb_dbs
export myLBSCRIPTS=/afs/cern.ch/lhcb/software/releases/LBSCRIPTS/prod/InstallArea/scripts
export SQLITEMASTER=/afs/cern.ch/lhcb/software/SQLiteMaster

echo '=========================================================================================================='
. $myLBSCRIPTS/LbLogin.sh
. $myLBSCRIPTS/SetupProject.sh LHCb --no-user
echo '=========================================================================================================='

echo
echo '********************************'
echo '* Welcome to CondDB DBS Pilot! *'
echo '********************************'
echo

pull_online=false
pull_offline=false
pull_upgrade_offline=false
repo_updated=false

startPublishingSwitch=$SQLITEMASTER/SQLDDDB/.startPublishing
startPublishingUpgradeSwitch=$SQLITEMASTER/SQLDDDB_Upgrade/.startPublishing

sqldddb_publishing=$SQLITEMASTER/SQLDDDB/.running
sqldddb_upgrade_publishing=$SQLITEMASTER/SQLDDDB_Upgrade/.running

################################################################ SQLDDDB ##################################################################################

# Check if ONLINE snapshot needs to be updated
~/lb_conddb_dbs/check_sqlite_conddb_heartbeat.py
if [ $? -eq 0 ] ; then pull_online=true ; fi

# Check if another publishing process is running
echo
echo 'Trying to lock SQLDDDB source..'
if [ ! -e $sqldddb_publishing ] ; then
    touch $sqldddb_publishing
    echo 'SQLDDDB source locked!'
    # Check if "Offline" SQLite partitions are forced to be published
    if [ -e $startPublishingSwitch ]; then pull_offline=true ; fi
    if $pull_online ; then
       echo '======================================'
       echo '>>> Time to make ONLINE snapshot! <<<'
       echo '======================================'
       echo "Using `which CondDB_UpdateOnlineSnapshots.py`"
       CondDB_UpdateOnlineSnapshots.py -d $SQLITEMASTER/SQLDDDB/ -b
    fi

    if $pull_online || $pull_offline ; then
       echo '============================================================'
       echo ">>> Time to publish CondDB! Offline: $pull_offline, Online: $pull_online. <<<"
       echo '============================================================'
       /afs/cern.ch/lhcb/software/DEV/DBASE/Det/SQLDDDB/scripts/CondDBAdmin_PublishSQLite.py -b -v -s $SQLITEMASTER/SQLDDDB
       repo_updated=true
    fi

    if [ -e $sqldddb_publishing ] ; then rm -f $sqldddb_publishing ; echo 'SQLDDDB source unlocked!'; fi
    
else
    echo '=============================================================='
    echo '>>> Another Pilot is publishing SQLDDDB! Skipping SQLDDDB. <<<'
    echo '=============================================================='
fi

################################################################ SQLDDDB Upgrade ##########################################################################
echo
echo 'Trying to lock SQLDDDB Upgrade source..'
if [ ! -e $sqldddb_upgrade_publishing ] ; then
    touch $sqldddb_upgrade_publishing
    echo 'SQLDDDB Upgrade source locked!'
    if [ -e $startPublishingUpgradeSwitch ]; then pull_upgrade_offline=true ; fi
    if $pull_upgrade_offline ; then
        echo '======================================='
        echo '>>> Time to publish CondDB Upgrade! <<<'
        echo '======================================='
        /afs/cern.ch/lhcb/software/DEV/DBASE/Det/SQLDDDB/scripts/CondDBAdmin_PublishSQLite.py -b -v -s $SQLITEMASTER/SQLDDDB_Upgrade -d $LHCBTAR/SQLite/SQLDDDB_Upgrade
        repo_updated=true
    fi

    if [ -e $sqldddb_upgrade_publishing ] ; then rm -f $sqldddb_upgrade_publishing ; echo 'SQLDDDB Upgrade source unlocked!'; fi
else
    echo '=============================================================================='
    echo '>>> Another Pilot is publishing SQLDDDB Upgrade! Skipping SQLDDDB Upgrade. <<<'
    echo '=============================================================================='
fi

############################################################ Updating AFS RA ###############################################################################

if $repo_updated ; then
    echo
    echo '=============================='
    echo '>>> Time to update AFS RA! <<<'
    echo '=============================='
    /afs/cern.ch/lhcb/software/DEV/DBASE/Det/SQLDDDB/cmt/Update.py --debug
else
    echo
    echo '======================================='
    echo '>>> Nothing was published by Pilot! <<<'
    echo '======================================='
fi

#echo '=========================================================================================================='
#/afs/cern.ch/user/i/ishapova/private/cron_assistants/CopyOnlineSnap2Dev.sh

echo
echo '**********************************'
echo '* Leaving CondDB DBS Pilot: Bye! *'
echo '**********************************'
echo


#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from sys import exit
from PyCool import cool
from time import time, localtime, strftime
from CondDBUI import CondDB
from imp import load_source

print "Checking whether there is a necessity to update ONLINE snapshots:"

path2sqldddbdotpy = os.path.join(os.environ['SQLDDDBROOT'], 'options',
                                 'SQLDDDB.py')
sqldddbdotpy = load_source('SQLDDDB', path2sqldddbdotpy)
ym_tuple = sqldddbdotpy.getLatestSnapshot()
sqlitemasterpath = os.path.join(os.environ['LHCBHOME'], 'software',
                                'SQLiteMaster', 'SQLDDDB', 'db')
if ym_tuple[1] is None:
    previousSQLiteDBPath = os.path.join(sqlitemasterpath,
                                        "ONLINE-%04d.db" % ym_tuple[0])
else:
    previousSQLiteDBPath = os.path.join(sqlitemasterpath,
                                        "ONLINE-%04d%02d.db" % ym_tuple)

print "Taking the latest available ONLINE snapshot: %s" % previousSQLiteDBPath
if not os.path.isfile(previousSQLiteDBPath):
    print "WARNING: The file '%s' doesn't exist. Triggering the snapshot.." % previousSQLiteDBPath
    exit(0)

dbSQLite = CondDB('sqlite_file:' + previousSQLiteDBPath + '/ONLINE')
SQLiteTick = dbSQLite.getXMLStringList('/Conditions/Online/LHCb/Tick', 0,
                                       cool.ValidityKeyMax)

if len(SQLiteTick) != 0:
    last_known_IOV_start = SQLiteTick[-1][1]
    dbORA = CondDB('CondDBOnline/ONLINE')
    ORATicks = dbORA.getXMLStringList('/Conditions/Online/LHCb/Tick',
                                      last_known_IOV_start,
                                      cool.ValidityKeyMax)
    new_IOV_start = ORATicks[-1][1]

    print "Latest SQLite Online HeartBeat insertion: %s" % strftime(
        "%a, %d %b %Y %H:%M:%S +0000", localtime(last_known_IOV_start * 1e-9))
    for tick in ORATicks[1:-1]:
        print "Intermediate Oracle Online HeartBeat insertion: %s" % strftime(
            "%a, %d %b %Y %H:%M:%S +0000", localtime(tick[1] * 1e-9))
    print "Latest Oracle Online HeartBeat insertion: %s" % strftime(
        "%a, %d %b %Y %H:%M:%S +0000", localtime(new_IOV_start * 1e-9))

    if new_IOV_start != last_known_IOV_start:
        exit(0)
    else:
        exit(1)
# In case the SQLite DB is malformed the list of IOVs may be empty
else:
    print "WARNING: the list of the IOVs for the Tick condition is empty. Possible reason: the file at '%s' is malformed" % previousSQLiteDBPath
    exit(1)

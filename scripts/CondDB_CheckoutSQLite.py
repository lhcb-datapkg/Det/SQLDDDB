#!/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__ = "Illya Shapoval <illya.shapoval@cern.ch>"
_LAYOUT_VERSION = '1.0'

import os, sys

# Map the site specific http proxy variable on urllib recognized one
# ( this makes the script aware of proxies around)
if not os.environ.get('http_proxy') and os.environ.get('VO_LHCB_PROXY_URL'):
    os.environ['http_proxy'] = os.environ['VO_LHCB_PROXY_URL']

from subprocess import Popen, PIPE
try:
    from os.path import relpath
except ImportError:
    # The function relpath has been introduced in Python 2.6.
    # This version is a backport
    def relpath(path, start=os.path.curdir):
        """Return a relative version of a path"""

        if not path:
            raise ValueError("no path specified")

        start_list = os.path.abspath(start).split(os.path.sep)
        path_list = os.path.abspath(path).split(os.path.sep)

        # Work out how much of the filepath is shared by start and path.
        i = len(os.path.commonprefix([start_list, path_list]))

        rel_list = [os.path.pardir] * (len(start_list) - i) + path_list[i:]
        if not rel_list:
            return os.path.curdir
        return os.path.join(*rel_list)


try:
    import xml.etree.ElementTree as ET
except ImportError:
    sys.path.append(
        os.path.join(
            os.path.realpath(os.path.dirname(__file__)), os.pardir, "python"))
    import ElementTree as ET
from random import uniform
from time import sleep
try:
    from hashlib import sha1
except ImportError:
    from sha import new as sha1
from re import match
from urllib2 import urlopen, HTTPError
from tempfile import mkdtemp, mkstemp

# Prepare local logger
import logging
if __name__ != '__main__': actor = __name__
else: actor = os.path.basename(sys.argv[0])
log = logging.getLogger(actor)


def _format_str(string, color_name, blink=False, underline=False):
    """Function to prettify strings."""
    color_vocab = {
        'black': 30,
        'red': 31,
        'green': 32,
        'yellow': 33,
        'blue': 34,
        'magenta': 35,
        'cyan': 36,
        'white': 37,
        '': ''
    }
    ##########################################################################
    if color_name in color_vocab:
        color_num = str(color_vocab.get(color_name)) + ';'
    else:
        log.warning(
            "I don't know the requested color name '%s'. Known are: %s."
            "\nUsing 'White'.." % (color_name, color_vocab.keys()))
        color_num = ''
    ##########################################################################
    if blink: blink = '5;'
    else: blink = ''
    ##########################################################################
    if underline:
        underline = '4;'
    else:
        underline = ''
    ##########################################################################
    return '\x1B[' + '%s%s%s49m' %(color_num, blink, underline) + string + \
           '\x1B['+ '0m'


def _installed(prog):
    """Check requested program is on the PATH (prog: str; program name)."""

    for path in os.environ["PATH"].split(os.pathsep):
        path2prog = os.path.join(path, prog)
        if os.path.exists(path2prog) and os.access(path2prog, os.X_OK):
            return True

    return False


def compute_checksums(path,
                      xml_elem=None,
                      dir_pattern='.',
                      file_pattern='.',
                      action_msg='Hashing',
                      chunksize=1048576):
    """Recursive function to compute checksums of a folder tree."""

    if xml_elem is not None and not xml_elem.get('source'):
        xml_elem.set('source', os.path.dirname(path))

    if os.path.isfile(path) and match(file_pattern, os.path.basename(path)):
        log.debug("%s '%s' .." % (action_msg, os.path.basename(path)))
        hashSumObj = sha1()
        f = open(path)
        chunk = f.read(chunksize)
        while chunk:
            hashSumObj.update(chunk)
            chunk = f.read(chunksize)
        f.close()

        if xml_elem is not None:
            ET.SubElement(
                xml_elem, 'file', {
                    'path': relpath(path, xml_elem.get('source')),
                    'sha1': hashSumObj.hexdigest()
                })
        else:
            return (path, hashSumObj.hexdigest())
    elif os.path.isdir(path) and match(dir_pattern,
                                       os.path.basename(path.rstrip(os.sep))):
        items = [item for item in os.listdir(path)]
        for item in items:
            compute_checksums(
                os.path.join(path, item), xml_elem, dir_pattern, file_pattern)


def indent(elem, level=0):
    """Indent an ElementTree instance to allow pretty print."""

    i = "\n" + level * "  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for child in elem:
            indent(child, level + 1)
        if not child.tail or not child.tail.strip():
            child.tail = i
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


def _cleanup(target):
    """Function to remove requested items specified as a list"""

    if not target: return
    removed = []

    from shutil import rmtree
    for item in target:
        try:
            if os.path.isfile(item):
                os.remove(item)
                removed.append(item)
            elif os.path.isdir(item):
                rmtree(item, ignore_errors=True)
                removed.append(item)
        except:
            log.error("Failed to remove '%s': %s" % (item, sys.exc_info()[:2]))
    if removed: log.debug("Garbage removed: %s" % removed)


def _getNotReferencedPoolFiles(path, poolfld='.pool'):
    """Get the pool files which are not referenced from any link in a path."""

    poolPath = os.path.join(path, poolfld)
    poolFiles = [
        os.path.realpath(os.path.join(poolPath, x))
        for x in os.listdir(poolPath) if not x.startswith('tmp-')
    ]
    unrefPoolFiles = list(poolFiles)

    for root, dirs, files in os.walk(path):
        for file in files:
            file = os.path.join(root, file)
            if os.path.islink(file) and os.path.realpath(
                    file) in unrefPoolFiles:
                unrefPoolFiles.remove(os.path.realpath(file))

    if unrefPoolFiles:
        log.debug("Unreferenced files in the pool are: %s" % unrefPoolFiles)
    return unrefPoolFiles


def _exit(files=None, retvalue=0):
    """Perform clean exit"""

    if not files: files = []
    log.info(
        _format_str("Stopping and rolling back the checkout ...", "green"))
    _cleanup(files)
    log.info(_format_str("Syncing not done!", "red"))
    return retvalue


def main(local_argv=sys.argv):
    try:
        # Configure the parser
        from optparse import OptionParser
        parser = OptionParser(
            usage="%prog [options]",
            description=
            """This script checks out CondDB files (SQLite DB and release notes files).

Following types of files, different from the latest released ones, are dealt:
A - a file, which was not modified by user but only in the latest release;
B - a file, different due to both user and new release changes;
C - a file, different only due to user changes.

If no 'f', or 'F', options are given the script updates only A-type files.""")

        parser.add_option(
            "-s",
            "--source",
            type="string",
            help="Source URL to checkout CondDB SQLite files from."
            " Default is http://lhcbproject.web.cern.ch/lhcbproject/dist/SQLite/SQLDDDB/."
        )
        parser.add_option(
            "-d",
            "--destination",
            type="string",
            help="Destination path to checkout CondDB SQLite files to."
            " Default is $LHCBRELEASES/SQLite/SQLDDDB/")
        parser.add_option(
            "-f",
            "--part-force",
            action="store_true",
            help="Acts like 'partial force': update A+B-type files.")
        parser.add_option(
            "-F",
            "--no-uuc",
            "--no-use-user-catalog-file",
            action="store_true",
            help="Acts like 'full force': update A+B+C-type files.")
        parser.add_option(
            "-y",
            "--batch-yes",
            action="store_true",
            help="Confirm the checkout in advance once changes are detected.")
        parser.add_option(
            "-n",
            "--batch-no",
            "--dry-run",
            action="store_true",
            help="Do not apply actual changes, just check them (dry-run).")
        parser.add_option(
            "-v", "--debug", action="store_true", help="Print DEBUG messages.")
        parser.add_option(
            "--vv",
            action="store_true",
            help="Print DEBUG messages and decompression details.")

        parser.set_default(
            "source",
            "http://lhcbproject.web.cern.ch/lhcbproject/dist/SQLite/SQLDDDB/")
        parser.set_default(
            "destination",
            os.path.join(os.environ["LHCBRELEASES"].split(os.pathsep)[0],
                         "SQLite", "SQLDDDB"))

        # Parse command line
        options, args = parser.parse_args(args=local_argv[1:])

        if options.batch_yes and options.batch_no:
            parser.error(
                "Use either 'batch-yes', or 'batch-no' - they are mutually exclusive"
            )

        sourceArea = options.source.rstrip('/') + '/'
        sourcePoolURL = sourceArea + 'pool' + os.sep
        sourceCatalogURL = sourceArea + 'catalogs' + os.sep

        # Backup setup for the module-level logger
        if options.debug or options.vv: verbosity = logging.DEBUG
        else: verbosity = logging.INFO
        if log.getEffectiveLevel() != verbosity:
            log.setLevel(verbosity)
        if not logging.getLogger().handlers:
            # Set global stream handler
            hndlr = logging.StreamHandler()
            hndlr.setFormatter(
                logging.Formatter('%(levelname)s: (%(name)s) %(message)s'))
            logging.getLogger().addHandler(hndlr)

        # Check flow control file
        try:
            ctrlFileSocket = urlopen(sourceArea + '.stopUpdating')
            log.debug("The '.stopUpdating' file is found at the repository.")
            log.info(
                "Syncing is not possible since the repository is currently in a NON-ACTIVE state. Please try again later."
            )
            ctrlFileSocket.close()
            return 0
        except HTTPError, error:
            # recognize the "Not Found" HTTP error
            if error.code == 404:
                log.debug("The repository is ACTIVE.")
            else:
                log.error(
                    "Unable to check the flow control switch at the repository, the reason is: %s. Exiting."
                    % error)
                return 1

        # Check the repository layout version
        try:
            versionFileSocket = urlopen(sourceArea + 'repository')
        except HTTPError, why:
            log.error("Failed to download 'repository' file with error: '%s'. "
                      "Checkout didn't start." % why)
            return 1
        foundRepoLayoutVersion = versionFileSocket.read().rstrip('\n')
        if foundRepoLayoutVersion != _LAYOUT_VERSION:
            log.error("Current repository layout ('%s') is not recognized "
                      "(latest known is '%s'). Exiting." %
                      (foundRepoLayoutVersion, _LAYOUT_VERSION))
            return 1

        # Get the latest release date-time
        try:
            currentFileSocket = urlopen(sourceArea + 'current')
        except HTTPError, why:
            log.error("Failed to download the 'current' file: '%s'. "
                      "Checkout didn't start." % why)
            return 1
        latestReleaseDateTime = currentFileSocket.read().rstrip('\n')
        if not match(r'^(\d{4})-(\d{2})-(\d{2})_(\d{2}):(\d{2}):(\d{2})$',
                     latestReleaseDateTime):
            log.error(
                "Fetched latest DB release date-time string at '%s' is not"
                " of an expected form. Checkout didn't start." %
                currentFileSocket.url)
            return 1
        currentFileSocket.close()

        # Get all released files info
        try:
            releasedCatalogFileSocket = urlopen(sourceCatalogURL +
                                                latestReleaseDateTime)
        except HTTPError, why:
            log.error("Failed to download '%s' catalog file: '%s'. "
                      "Checkout didn't start." % (latestReleaseDateTime, why))
            return 1
        releasedCatalogDOM = ET.parse(releasedCatalogFileSocket)
        releasedCatalogFileSocket.close()
        userCatalogDOM = releasedCatalogDOM
        userrootattribs = userCatalogDOM.getroot().attrib
        userrootattribs['first_install_state'] = latestReleaseDateTime
        userrootattribs['current_state'] = latestReleaseDateTime
        if not (userrootattribs.get('date') is None):
            del userrootattribs['date']
        releasedCatalogFiles = {}
        for elem in releasedCatalogDOM.findall('file'):
            path = elem.get('path')
            if path: releasedCatalogFiles[path] = elem

        # Check destination path
        destinationArea = options.destination.rstrip('/') + os.sep
        if not os.path.isdir(destinationArea):
            log.error(
                "Destination path '%s' doesn't exist. Checkout didn't start." %
                destinationArea)
            return 1

        # Check and, if needed, create the pool and the destination tree folders
        destPoolPath = os.path.join(destinationArea, '.pool')
        if not os.path.isdir(destPoolPath):
            os.mkdir(destPoolPath)
        else:
            # Remove not referenced pool files
            _cleanup(_getNotReferencedPoolFiles(destinationArea))
        destTreeNonEmpty = False
        for path in releasedCatalogFiles.keys():
            fileDir = os.path.split(path)[0].lstrip(os.sep)
            folders = fileDir.split(os.sep)
            checkPath = destinationArea
            for f in folders:
                checkPath = os.path.join(checkPath, f)
                if not os.path.isdir(checkPath):
                    try:
                        os.mkdir(checkPath)
                    except OSError, why:
                        log.error(
                            "Failed to create the destination sub-directories: %s"
                            % why)
                        return _exit([], 1)
                # Check if the destination folder tree contains any files
                elif not destTreeNonEmpty:
                    for res in os.walk(checkPath):
                        if res[2]:
                            destTreeNonEmpty = True
                            break

        # Get all user files info
        userCatalogLinkPath = os.path.join(destinationArea, 'catalog')
        if options.part_force and not os.path.isfile(userCatalogLinkPath):
            options.no_uuc = True
        if options.no_uuc:
            log.warning(
                "All user modified files will be overwritten with the released ones"
            )

        # The types all files will be classified as
        files2add = []
        files2update = []
        user_modified_files = []
        files2remove = []

        # Inventory all files according to the action types defined just above
        if os.path.isfile(userCatalogLinkPath) and not options.no_uuc:

            userCatalogRoot = ET.parse(userCatalogLinkPath).getroot()
            userCatalogFiles = {}
            for elem in userCatalogRoot.findall('file'):
                if 'path' in elem.attrib:
                    userCatalogFiles[elem.get('path')] = elem

            # Ignore identical files while downloading and update user's checksums
            # values of the ones to update
            for elem in userCatalogFiles.values():
                elem_path = elem.get('path')
                full_elem_path = os.path.join(destinationArea, elem_path)
                released_file_elem = releasedCatalogFiles.get(elem_path)
                elem_sha1 = elem.get('sha1')

                # If actual file was lost (although is in the catalog file)
                if not os.path.isfile(full_elem_path):
                    # Restore it using released one -> files2add
                    if released_file_elem is not None:
                        elem.set('sha1', released_file_elem.get('sha1'))
                        elem.set('size', released_file_elem.get('size'))
                        files2add.append(elem_path)
                    # If it was also removed from latest release -> files2remove
                    else:
                        userCatalogRoot.remove(elem)
                        files2remove.append(elem_path)
                    continue

                # If the file was removed from the release set -> files2remove
                if released_file_elem is None:
                    userCatalogRoot.remove(elem)
                    files2remove.append(elem_path)
                    continue

                if elem_sha1 != released_file_elem.get('sha1'):
                    # user's file is modified by user himself
                    if (os.stat(full_elem_path).st_mtime >
                            os.stat(userCatalogLinkPath).st_mtime and
                            elem_sha1 != compute_checksums(full_elem_path)[1]):
                        user_modified_files.append(elem_path)
                        # force user's file update only if requested -> files2update
                        if options.part_force:
                            files2update.append(elem_path)
                            elem.set('sha1', released_file_elem.get('sha1'))
                            elem.set('size', released_file_elem.get('size'))
                    # user's file wasn't changed by user and needs to be updated
                    # with the released one -> files2update
                    else:
                        files2update.append(elem_path)
                        elem.set('sha1', released_file_elem.get('sha1'))
                        elem.set('size', released_file_elem.get('size'))

            # Add new file if any is found in the latest release -> files2add
            for file in releasedCatalogFiles.keys():
                if file not in userCatalogFiles:
                    userCatalogRoot.append(releasedCatalogFiles[file])
                    files2add.append(file)

            if not (userCatalogRoot.get('date') is None):
                attribs = userCatalogRoot.attrib
                attribs['first_install_state'] = attribs.get('date')
                del attribs['date']
            userCatalogRoot.set('current_state', latestReleaseDateTime)

            indent(userCatalogRoot)
            userCatalogDOM = ET.ElementTree(userCatalogRoot)

        # if there is no catalog file (or it's requested to be ignored), but
        # the destination folder tree is not empty
        elif destTreeNonEmpty:
            log.warning(
                _format_str(
                    "No user's 'catalog' file found or requested to be ignored, => "
                    "all found user's files will be updated if "
                    "different from the latest released ones!", "green"))
            # Ignore identical files while downloading
            for elem in releasedCatalogFiles.values():
                elem_path = elem.get('path')
                elem_sha1 = elem.get('sha1')
                user_elem_path = os.path.join(destinationArea, elem_path)
                # Re-hash found candidate for an update and check if it's changed
                if os.path.isfile(user_elem_path):
                    user_sha1 = compute_checksums(user_elem_path)[1]
                    if elem_sha1 != user_sha1:
                        files2update.append(elem_path)
                # Add missing file to download queue
                else:
                    files2add.append(elem_path)
        else:
            # Fresh install
            files2add = releasedCatalogFiles.keys()

        log.debug("Source: %s" % _format_str(sourceArea, "yellow"))
        log.debug("Destination: %s" % _format_str(destinationArea, "yellow"))
        log.debug("File(s) to add: %s" % _format_str(str(files2add), "yellow"))
        log.debug(
            "File(s) to update: %s" % _format_str(str(files2update), "yellow"))
        log.debug(
            "File(s) to remove: %s" % _format_str(str(files2remove), "yellow"))
        if user_modified_files:
            if not options.part_force:
                log.debug(
                    "File(s) seem to be modified by user (will not be updated"
                    " with the released one(s)): %s" % _format_str(
                        str(user_modified_files), "red"))
            else:
                log.warning(
                    "Looks like there exist(s) file(s) modified by user "
                    "(due to '-f' option provided they/it will be overwritten): %s"
                    % _format_str(str(user_modified_files), "red"))

        if not files2update and not files2add and not files2remove:
            log.info("Nothing to do! Package is up-to-date. Exiting.")
            if not os.path.isfile(userCatalogLinkPath):
                f = open(os.path.join(destPoolPath, 'user_catalog'), 'w')
                userCatalogDOM.write(f, encoding="UTF-8")
                f.close()
                if os.path.lexists(userCatalogLinkPath):
                    os.remove(userCatalogLinkPath)
                os.symlink(
                    relpath(
                        os.path.join(destPoolPath, 'user_catalog'),
                        os.path.dirname(userCatalogLinkPath)),
                    userCatalogLinkPath)
                log.debug("The destination catalog file is resurrected.")
            return 0

        # Estimate space needed for the checkout
        checkout_size = 0
        # Sum new files
        for (path, elem) in releasedCatalogFiles.items():
            if path in files2update + files2add:
                checkout_size += int(elem.get('size').rstrip('B'))
        # Subtract files to be removed
        for file in files2remove:
            elem = userCatalogFiles.get(file)
            if elem is not None:
                checkout_size -= int(elem.get('size').rstrip('B'))

        log.debug("Space needed at the destination: ~ %.3f MB" %
                  (checkout_size / 1048576.))

        # Check available space at the destination
        avail_space = None
        try:
            if destinationArea.startswith("/afs"):
                checkSpaceProc = Popen(
                    ['fs', 'listquota', '-path', destinationArea], stdout=PIPE)
                # get the 2nd and 3rd items of the second line of the standard output
                quota, used = checkSpaceProc.communicate()[0].splitlines(
                )[1].split()[1:3]
                avail_space = (int(quota) - int(used)) / 1024.  # in megabytes
            else:
                checkSpaceProc = Popen(['df', '-P', destinationArea],
                                       stdout=PIPE)
                # get the second item of the second line of the standard output
                avail = checkSpaceProc.communicate()[0].splitlines()[1].split(
                )[1]
                avail_space = int(avail) / 1024.  # in megabytes
            log.debug(
                "Available space at the destination: %.3f MB" % avail_space)
        except Exception, why:
            log.debug(why)
            log.debug(
                "Available space at the destination: estimation wasn't successful"
            )

        if avail_space and (checkout_size / 1048576. >= avail_space):
            log.error(
                "There is not enough space at the destination to checkout")
            return 1

        if options.batch_yes: ans = "Yes"
        elif options.batch_no: ans = "No"
        else: ans = None

        while ans is None:
            ans = raw_input(
                "\nDo you really want to start syncing with the release area (Yes,[No])? "
            )
            if not ans: ans = "No"
            if ans not in ["Yes", "No"]:
                print "You have to type exactly 'Yes' or 'No'"
                ans = None

        if ans == "No":
            log.info("Syncing is canceled by the user cleanly. Exiting.")
            return 0
    except KeyboardInterrupt:
        log.info("Aborted by user cleanly: nothing is updated. Exiting.")
        return 0

    try:
        ########################################################################
        # Start syncing
        log.info(_format_str("Syncing ...", "green"))

        fileHierarchyIsUntouched = True
        userCatalogIsUntouched = True

        files2download = files2update + files2add
        downloaded_files = []

        userCatalogPoolPath = os.path.join(destPoolPath, latestReleaseDateTime)
        linkageDict = {userCatalogPoolPath: userCatalogLinkPath}
        destPoolTMPPath = mkdtemp(prefix='tmp-', dir=destPoolPath)

        for file in files2download:
            repositFileName = releasedCatalogFiles[file].get('sha1')
            downloaded = False
            download_attempts = 0
            outLinkFile = os.path.join(destinationArea, file)
            outPoolFile = os.path.join(destPoolPath, repositFileName)
            linkageDict[outPoolFile] = outLinkFile
            outPoolTMPFile = os.path.join(destPoolTMPPath,
                                          repositFileName) + '.bz2'
            downloaded_files.append(outPoolTMPFile)

            # Download the file (with 9 extra attempts if an attempt is unsuccessful)
            while not downloaded and download_attempts < 10:
                log.debug(
                    "Downloading '%s.bz2' (%s) .." % (repositFileName, file))
                try:
                    download_attempts += 1
                    distrSocket = urlopen(sourcePoolURL + repositFileName)
                    if distrSocket.code == 200: downloaded = True
                    else: continue
                    oF = open(outPoolTMPFile, 'wb')
                    oF.write(distrSocket.read())
                    oF.close()
                except HTTPError, why:
                    if download_attempts == 10: break
                    sleep_time = 3 + uniform(-1, 1)
                    log.error("Failed to download: '%s'."
                              " Performing %i/10 attempt in %fs .." %
                              (why, download_attempts + 1, sleep_time))
                    sleep(sleep_time)
                if 'distrSocket' in locals(): distrSocket.close()
            # If any of files failed to be downloaded - stop everything and roll back the checkout
            if not downloaded:
                log.error(
                    _format_str(
                        "All download attempts on '%s' have failed" % file,
                        "red"))
                return _exit([destPoolTMPPath], 1)

        # Decompress downloaded files and check their integrity
        if _installed('pbzip2'): compress_engine = 'pbzip2'
        elif _installed('bzip2'): compress_engine = 'bzip2'
        else:
            log.error(
                _format_str(
                    "Looks like we need (p)bzip2 decompressor "
                    "but it seems to be not installed", "red"))
            return _exit([destPoolTMPPath], 1)

        if options.vv: compr_options = '-dfv'
        else: compr_options = '-df'

        decompressed_files = []
        for inFile in downloaded_files:
            fileName = os.path.basename(inFile).replace('.bz2', '')
            uncomprFile = inFile.replace('.bz2', '')
            log.debug("Decompressing '%s' .." % (os.path.basename(inFile)))
            decompressed_files.append(uncomprFile)
            decomprProc = Popen([compress_engine, compr_options, inFile])
            decomprProc.wait()
            if decomprProc.returncode != 0:
                log.error(
                    _format_str(
                        "Decompression failed with return code %i" %
                        decomprProc.returncode, "red"))
                return _exit([destPoolTMPPath], 1)

            # Check decompressed files' integrity
            sha1sum = compute_checksums(uncomprFile, action_msg='Checking')[1]
            if sha1sum != os.path.basename(uncomprFile):
                log.error(
                    _format_str(
                        "Transaction test for the '%s' has failed" % fileName,
                        "red"))
                return _exit([destPoolTMPPath], 1)

        #Write temporary catalog to tmp pool directory
        userCatalogPoolTMPPath = os.path.join(destPoolTMPPath,
                                              latestReleaseDateTime)
        fcatal = open(userCatalogPoolTMPPath, 'wb')
        userCatalogDOM.write(fcatal, encoding="UTF-8")
        fcatal.close()

        # Making temporary checked out files persistent and (re)linking to them the hierarchy
        log.debug("Discovering new files to the file hierarchy ..")
        fileHierarchyIsUntouched = False
        for tmpfile in decompressed_files:
            poolFile = os.path.join(destPoolPath, os.path.basename(tmpfile))
            os.rename(tmpfile, poolFile)
            linkFile = linkageDict[poolFile]
            if os.path.lexists(linkFile): os.remove(linkFile)
            os.symlink(relpath(poolFile, os.path.dirname(linkFile)), linkFile)

        if files2remove:
            log.debug(
                "Removing obsolete files (removed from the latest release) ..")
            for file in files2remove:
                full_file_path = os.path.join(destinationArea, file)
                if os.path.isfile(full_file_path): os.remove(full_file_path)

        # Discover the catalog
        os.rename(userCatalogPoolTMPPath, userCatalogPoolPath)
        userCatalogIsUntouched = False
        if os.path.lexists(userCatalogLinkPath): os.remove(userCatalogLinkPath)
        os.symlink(
            relpath(userCatalogPoolPath, os.path.dirname(userCatalogLinkPath)),
            userCatalogLinkPath)
        log.debug("Destination catalog is updated.")

        #Cleanup actions
        _cleanup([destPoolTMPPath])
        log.info(_format_str("Done!", "green"))

        return 0

    except (Exception, KeyboardInterrupt), why:
        if isinstance(why, KeyboardInterrupt):
            log.info(_format_str("Aborted by user.", "green"))
        else:
            log.error(why)

        if fileHierarchyIsUntouched:
            log.warning(
                _format_str("File hierarchy has not been touched.", "red"))
        else:
            log.warning(
                _format_str(
                    "File hierarchy might have been partially or fully updated.",
                    "red"))

        if userCatalogIsUntouched:
            log.warning(
                _format_str("Destination catalog has not been updated.",
                            "red"))
        else:
            log.warning(
                _format_str("Destination catalog might have been updated.",
                            "red"))

        # Cleanup actions
        if 'decomprProc' in locals() and decomprProc.returncode is None:
            if sys.version_info >= (2, 6): decomprProc.terminate()
            else: decomprProc.wait()
        if 'distrSocket' in locals(): distrSocket.close()

        if 'destPoolTMPPath' in locals(): _cleanup([destPoolTMPPath])
        log.info(_format_str("Done!", "green"))

        return 1


if __name__ == '__main__':
    sys.exit(main())

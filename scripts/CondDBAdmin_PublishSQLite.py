#!/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__ = "Illya Shapoval <illya.shapoval@cern.ch>"

_LAYOUT_VERSION = '1.0'

from subprocess import Popen, PIPE
import xml.etree.ElementTree as ET
from shutil import move, copy2, rmtree, copystat
import time
from hashlib import sha1
from shlex import split
from re import match
import os


def _format_str(string, color_name, blink=False, underline=False):
    """Function to prettify strings."""
    color_vocab = {
        'black': 30,
        'red': 31,
        'green': 32,
        'yellow': 33,
        'blue': 34,
        'magenta': 35,
        'cyan': 36,
        'white': 37,
        '': ''
    }
    ##########################################################################
    if color_name in color_vocab:
        color_num = str(color_vocab.get(color_name)) + ';'
    else:
        log.info("I don't know the requested color name '%s'. Known are: %s."
                 "\nUsing 'White'.." % (color_name, color_vocab.keys()))
        color_num = ''
    ##########################################################################
    if blink: blink = '5;'
    else: blink = ''
    ##########################################################################
    if underline:
        underline = '4;'
    else:
        underline = ''
    ##########################################################################
    return '\x1B[' + '%s%s%s49m' %(color_num, blink, underline) + string + \
           '\x1B['+ '0m'


def _cleanup(target):
    """Function to remove requested items specified as a list"""

    removed = []
    from shutil import rmtree
    for item in target:
        try:
            if os.path.isfile(item):
                os.remove(item)
                removed.append(item)
            elif os.path.isdir(item):
                rmtree(item, ignore_errors=True)
                removed.append(item)
        except:
            log.error("Failed to remove '%s': %s" % (item, sys.exc_info()[:2]))
    if removed: log.debug("Garbage removed: %s" % removed)


def _installed(prog):
    """Check requested program is on the PATH (prog: str; program name)."""

    for path in os.environ["PATH"].split(os.pathsep):
        path2prog = os.path.join(path, prog)
        if os.path.exists(path2prog) and os.access(path2prog, os.X_OK):
            return True

    return False


def getLatestPublished(path):
    """Get latest published CondDB full path according to YYYY-MM-DD_HH:MM:SS pattern."""

    published = [
        fld for fld in os.listdir(path)
        if match(r'^(\d{4})-(\d{2})-(\d{2})_(\d{2}):(\d{2}):(\d{2})$', fld)
    ]

    if published: return max(published)
    else: return None


def computeHash(path, chunksize=1048576):

    log.info("Hashing    : '%s' .." % os.path.basename(path))
    hashSumObj = sha1()
    with open(path) as f:
        chunk = f.read(chunksize)
        while chunk:
            hashSumObj.update(chunk)
            chunk = f.read(chunksize)
    return hashSumObj.hexdigest()


def buildCatalog(path,
                 xml_elem=None,
                 ref_xml_elem=None,
                 dir_pattern='.',
                 file_pattern='.'):
    """Recursive function to build catalog of a folder tree."""

    if xml_elem is not None and not xml_elem.get('source'):
        xml_elem.set('source', os.path.dirname(path))

    if os.path.isfile(path) and match(file_pattern, os.path.basename(path)):
        ref_date_str = '1970-01-01_00:00:00'
        if ref_xml_elem is not None: ref_date_str = ref_xml_elem.get('date')
        ref_time = time.mktime(
            time.strptime(ref_date_str, '%Y-%m-%d_%H:%M:%S'))
        if os.path.getmtime(path) < ref_time:
            fileFoundAtRepo = False
            for file_elem in ref_xml_elem.findall('file'):
                publishedFilePath = file_elem.get('path')
                if publishedFilePath == os.path.relpath(
                        path, xml_elem.get('source')):
                    log.info("Unmodified : '%s'" % os.path.basename(path))
                    hashSum = file_elem.get('sha1')
                    fileFoundAtRepo = True
                    break
            if not fileFoundAtRepo:
                hashSum = computeHash(path)
        else:
            hashSum = computeHash(path)

        if xml_elem is not None:
            ET.SubElement(
                xml_elem, 'file', {
                    'path': os.path.relpath(path, xml_elem.get('source')),
                    'sha1': hashSum
                })
        else:
            return (path, hashSum)
    elif os.path.isdir(path) and match(dir_pattern,
                                       os.path.basename(path.rstrip(os.sep))):
        items = [item for item in os.listdir(path)]
        for item in items:
            buildCatalog(
                os.path.join(path, item), xml_elem, ref_xml_elem, dir_pattern,
                file_pattern)


def indent(elem, level=0):
    """Indent an ElementTree instance to allow pretty print."""

    i = "\n" + level * "  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for child in elem:
            indent(child, level + 1)
        if not child.tail or not child.tail.strip():
            child.tail = i
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


def main():
    # Configure the parser
    from optparse import OptionParser
    parser = OptionParser(
        usage="%prog [options]",
        description=
        """This script is an automatic LHCb software publishing and archival tool.
It is constructed to be used mostly for the heavy-weight database packages.
""")

    parser.add_option(
        "-s",
        "--source",
        type="string",
        help="Source path to publish CondDB SQLite files from ('db' and 'doc')."
        " Default is $LHCBDEV/DBASE/Det/SQLDDDB/")
    parser.add_option(
        "-d",
        "--destination",
        type="string",
        help="Destination area to publish CondDB SQLite files to."
        " Default is $LHCBTAR/SQLite/SQLDDDB/.")
    parser.add_option(
        "--dir-pattern",
        type="string",
        help=
        "Python regular expression which matches the directories to publish."
        " Default is SQLDDDB configuration.")
    parser.add_option(
        "--file-pattern",
        type="string",
        help="Python regular expression which matches the files to publish."
        " Default is SQLDDDB configuration.")
    parser.add_option(
        "--compress-engine",
        type="string",
        help="Engine to compress the files with. Default: '(p)bzip2'."
        " Currently only those are supported.")
    parser.add_option(
        "-b",
        "--batch",
        action="store_true",
        help="Publish without affirmation request.")
    parser.add_option(
        "--backup-depth",
        type="int",
        help="Number of backups to leave at destination. Default: 100")
    parser.add_option(
        "-f",
        "--force",
        action="store_true",
        help="Force re-hashing of a source file tree.")
    parser.add_option(
        "-v",
        "--verbose",
        action="store_true",
        help="Increase verbosity level.")

    parser.set_default(
        "source", os.path.join(os.environ["LHCBDEV"], "DBASE", "Det",
                               "SQLDDDB"))
    parser.set_default(
        "destination", os.path.join(os.environ["LHCBTAR"], "SQLite",
                                    "SQLDDDB"))
    parser.set_default("compress_engine", "pbzip2")
    parser.set_default("dir_pattern", "SQLDDDB|db|doc")
    parser.set_default(
        "file_pattern",
        "^[\w-]+\.db$|release_notes.xml|release_notes.xsd|release_notes.xsl")
    parser.set_default("backup_depth", 100)

    # Parse command line
    options, args = parser.parse_args()

    # Prepare local logger
    import logging
    global log
    log = logging.getLogger(parser.prog or os.path.basename(sys.argv[0]))
    log.setLevel(logging.DEBUG)

    # Set the global stream handler
    hndlr = logging.StreamHandler()
    hndlr.setFormatter(
        logging.Formatter('%(levelname)s: (%(name)s) %(message)s'))
    logging.getLogger().handlers = [hndlr]

    if options.dir_pattern and options.file_pattern:
        dirPattern = options.dir_pattern
        filePattern = options.file_pattern
    elif options.dir_pattern or options.file_pattern:
        parser.error(
            "Please provide both directory and file patterns to publish.")

    # Check source and destination
    sourceArea = options.source.rstrip('/') + os.sep
    publishArea = options.destination.rstrip('/') + os.sep
    if not os.path.isdir(sourceArea):
        log.error("Source path '%s' doesn't exist. Exiting." % sourceArea)
        return 1
    if not os.path.isdir(publishArea):
        log.error(
            "Destination path '%s' doesn't exist. Exiting." % publishArea)
        return 1
    publishPoolPath = publishArea + 'pool' + os.sep
    if not os.path.isdir(publishPoolPath):
        try:
            os.makedirs(publishPoolPath)
        except Exeption as ex:
            log.error(
                "Failed to create the pool directory '%s' for the compressed files."
                % publishPoolPath)
            log.error(ex)
            return 1
    publishCatalogPath = publishArea + 'catalogs' + os.sep
    if not os.path.isdir(publishCatalogPath):
        try:
            os.makedirs(publishCatalogPath)
        except Exeption as ex:
            log.error(
                "Failed to create the pool directory '%s' for the compressed files."
                % publishCatalogPath)
            log.error(ex)
            return 1

    # Form publication subfolder by datetime
    currentDateTime = time.strftime('%Y-%m-%d_%H:%M:%S')
    latestPublished = getLatestPublished(publishCatalogPath)
    if latestPublished:
        previousPublishedCatalog = os.path.join(publishCatalogPath,
                                                latestPublished)
    else:
        previousPublishedCatalog = None

    # Check compression engine for availability
    compress_engine = options.compress_engine
    if compress_engine not in ['bzip2', 'pbzip2']:
        log.error(
            "Requested compression engine is not supported by the script.")
        return 1
    if not _installed(compress_engine):
        alternat_compress_engine = [
            p for p in ['bzip2', 'pbzip2'] if p != compress_engine
        ][0]
        if _installed(alternat_compress_engine):
            log.warning("'%s' is not available, switching to '%s'" %
                        (compress_engine, alternat_compress_engine))
            compress_engine = alternat_compress_engine
        else:
            log.error(
                "No 'bzip2' or 'pbzip2' compression engines found in $PATH")
            return 1

    log.info("Source: %s" % _format_str(sourceArea, "yellow"))
    log.info("Destination: %s" % _format_str(publishArea, "yellow"))
    log.info("Previous published catalog: %s" % _format_str(
        str(previousPublishedCatalog), "cyan"))
    log.info("Compression: active (%s)" % compress_engine)

    # Check flow control file
    startfilepath = sourceArea.rstrip('/') + os.sep + ".startPublishing"
    stopfilepath = sourceArea.rstrip('/') + os.sep + ".stopPublishing"
    isstartfile = os.path.isfile(startfilepath)
    isstopfile = os.path.isfile(stopfilepath)
    if isstopfile:
        log.info(
            "Syncing canceled due to the existence of stop file at the source area."
        )
        if isstartfile:
            os.remove(startfilepath)
        return 0

    ans = 'Yes' if options.batch else None
    while ans is None:
        ans = raw_input(
            "\nDo you really want to synchronize SQLite db files (Yes,[No])? ")
        if not ans: ans = "No"
        if ans not in ["Yes", "No"]:
            print "You have to type exactly 'Yes' or 'No'"
            ans = None

    if ans == "No":
        log.info("Syncing canceled.")
        return 0

    try:
        # Clean up the publishing area
        found_catalogs = os.listdir(publishCatalogPath)
        found_catalogs.sort()
        allBackups = len(found_catalogs)
        if allBackups > options.backup_depth:
            unneededBackups = allBackups - options.backup_depth
            catalogs2remove = map(
                lambda x: os.path.join(publishCatalogPath, x),
                found_catalogs[:unneededBackups])
            candidates2remove = {}
            for catalog in catalogs2remove:
                candidates2remove[catalog] = []
                with open(catalog) as f:
                    catalogRoot = ET.parse(f).getroot()
                    for fileElem in catalogRoot.findall('file'):
                        sha1 = fileElem.get('sha1')
                        candidates2remove[catalog].append(fileElem.get('sha1'))

            # Check if the files to be removed are not used in the remaining backups
            catalogs2leave = map(lambda x: os.path.join(publishCatalogPath, x),
                                 found_catalogs[unneededBackups:])
            for catalog in catalogs2leave:
                with open(catalog) as f:
                    catalogRoot = ET.parse(f).getroot()
                    for fileElem in catalogRoot.findall('file'):
                        sha1 = fileElem.get('sha1')
                        for key in candidates2remove:
                            if sha1 in candidates2remove[key]:
                                candidates2remove[key].remove(sha1)
            files2cleanup = []
            for key in candidates2remove:
                files2cleanup.append(
                    [os.path.join(publishCatalogPath, key)] +
                    map(lambda x: os.path.join(publishPoolPath, x),
                        candidates2remove[key]))
            for backup in files2cleanup:
                _cleanup(backup)

        # Form the catalog of the files to be published
        dirName = os.path.basename(sourceArea.rstrip(os.sep))
        newCatalogRoot = ET.Element('catalog', {
            'name': dirName,
            'date': currentDateTime
        })
        dom = ET.ElementTree(element=newCatalogRoot)

        if previousPublishedCatalog and os.path.isfile(
                previousPublishedCatalog) and not options.force:
            previousCatalogRoot = ET.parse(previousPublishedCatalog).getroot()
        else:
            previousCatalogRoot = None
        buildCatalog(
            sourceArea,
            newCatalogRoot,
            previousCatalogRoot,
            dir_pattern=dirPattern,
            file_pattern=filePattern)
        del newCatalogRoot.attrib['source']  # was needed for technical reasons

        files2PublishDict = {}
        total_size = 0  # total size of uncompressed files in bytes
        for file_elem in newCatalogRoot.findall('file'):
            path = file_elem.get('path')
            file_size = os.path.getsize(os.path.join(sourceArea, path))
            total_size += file_size
            files2PublishDict[path] = file_elem
            file_elem.set('size', str(file_size) + 'B')
        newCatalogRoot.set('tsize', str(total_size) + 'B')
        indent(newCatalogRoot)

        # Decide which files are really different from the ones previously published
        previousPublishedFiles = {}
        modifiedFiles = []
        identicalFiles = []
        if previousCatalogRoot is not None:
            for file_elem in previousCatalogRoot.findall('file'):
                previousPublishedFiles[file_elem.get('path')] = file_elem
            for file_elem in newCatalogRoot.findall('file'):
                path = file_elem.get('path')
                prevPublFileElem = previousPublishedFiles.get(path)
                if prevPublFileElem is None \
                   or file_elem.get('sha1') != prevPublFileElem.get('sha1'):
                    modifiedFiles.append(path)
                else:
                    identicalFiles.append(path)
        else:
            modifiedFiles = files2PublishDict.keys()

        # Check space needed to publish the files
        space_needed = 0.
        for file in modifiedFiles:
            file = os.path.join(sourceArea, file)
            space_needed += os.path.getsize(file)  # in bytes
        space_needed = space_needed / 15.  #just some heuristic estimate with a generous safety margin
        log.info("Space needed at the destination: ~%.3f MB" %
                 (space_needed / 1048576.))

        # Check available space at destination
        avail_space = None
        try:
            if 'afs' in publishArea:
                checkSpaceProc = Popen(['fs', 'lq', publishArea], stdout=PIPE)
                checkSpaceProc.wait()
                output = checkSpaceProc.communicate()[0].split()
                avail_space = (
                    int(output[7]) - int(output[8])) / 1024.  # in megabytes
            else:
                checkSpaceProc = Popen(['df', publishArea], stdout=PIPE)
                checkSpaceProc.wait()
                output = checkSpaceProc.communicate()[0].split()
                avail_space = int(output[10]) / 1024.  # in megabytes
            log.info(
                "Available space at the destination: %.3f MB" % avail_space)
        except Exception as ex:
            log.debug(ex)
            log.info(
                "Available space at the destination: estimation wasn't successful"
            )

        #Set the minimum number of backups at 20
        min_backup_depth = 20

        if avail_space and (space_needed / 1048576. >= avail_space):
            log.warning(
                "There is not enough space at the destination to publish the files."
            )
            log.warning("Need to clean up some backups.")
            space_to_clear = space_needed - int(avail_space * 1048576.)

            candidates2clearup = {}
            space_cleared = 0

            # Keep a minimum number of backups
            if (allBackups > min_backup_depth):
                extraBackups = allBackups - min_backup_depth
                allcatalogs = map(
                    lambda x: os.path.join(publishCatalogPath, x),
                    found_catalogs)
                extracatalogs = allcatalogs[:extraBackups]

                # build up a list for all the files to be moved
                for catalog in extracatalogs:
                    candidates2clearup[catalog] = []
                    restcatalogs = allcatalogs[allcatalogs.index(catalog) + 1:]
                    with open(catalog) as f:
                        catalogRoot = ET.parse(f).getroot()
                        for fileElem in catalogRoot.findall('file'):
                            sha1 = fileElem.get('sha1')
                            candidates2clearup[catalog].append(
                                fileElem.get('sha1'))

                    # Check whether any listed file is used in the later backup, and remove the found file from the list
                    for catalog2 in restcatalogs:
                        with open(catalog2) as f:
                            catalogRoot = ET.parse(f).getroot()
                            for fileElem in catalogRoot.findall('file'):
                                sha1 = fileElem.get('sha1')
                                filesize = fileElem.get('size')
                                filesize = int(filesize.rstrip('B'))
                                if sha1 in candidates2clearup[catalog]:
                                    candidates2clearup[catalog].remove(sha1)

                    #  Calculate how much disk space will be freed after cleaning up this catalog
                    for file in [catalog] + map(
                            lambda x: os.path.join(publishPoolPath, x),
                            candidates2clearup[catalog]):
                        if os.path.isfile(file):
                            space_cleared += os.path.getsize(file)

                    # If enough free space is achieved, stop scanning for the next catalog
                    if space_cleared >= space_to_clear:
                        break

            # Check whether we could achieve enough free space
            if space_cleared < space_to_clear:
                #               log.error("There is not enough space at the destination to publish the files.")
                log.error(
                    "Not possible to free enough space as a minimum of %d back-ups are needed."
                    % min_backup_depth)
                return 1

            # Finalize the list of backup files to delete for more space
            files2freespace = []
            for key in candidates2clearup:
                files2freespace.append(
                    [os.path.join(publishCatalogPath, key)] +
                    map(lambda x: os.path.join(publishPoolPath, x),
                        candidates2clearup[key]))

            for backup in files2freespace:
                _cleanup(backup)

        ########################################################################
        # Compress and publish the files
        outFiles = []
        if modifiedFiles:
            log.info(
                _format_str("Compressing and Publishing the files ...",
                            "green"))

            # So far there is only (p)bzip2 compressor
            if options.verbose: compressComLine = [compress_engine] + ['-kcv']
            else: compressComLine = [compress_engine] + ['-kc']

            # Compress files (output files are renamed to their hashsums)
            for file in modifiedFiles:
                inFile = os.path.join(sourceArea, file)
                outFile = os.path.join(publishPoolPath,
                                       files2PublishDict.get(file).get('sha1'))
                outFiles.append(outFile)
                with open(outFile, 'wb') as f:
                    comprProc = Popen(compressComLine + [inFile], stdout=f)
                    comprProc.wait()
                if comprProc.returncode != 0:
                    log.error(
                        _format_str(
                            "Compression failed with return code %i." %
                            comprProc.returncode, "red"))
                    _cleanup(outFiles)
                    return 1
                copystat(inFile, outFile)
        else:
            log.info(_format_str("No changes found to be published.", "green"))
            if os.path.exists(startfilepath):
                os.remove(startfilepath)
                log.debug(
                    _format_str(
                        "Flow control switch file '%s' deleted." %
                        startfilepath, "green"))
            return 0

        # Create the catalog file
        newCatalogFilePath = publishCatalogPath + currentDateTime
        outFiles.append(newCatalogFilePath)
        with open(newCatalogFilePath, 'w') as f:
            dom.write(f, encoding="UTF-8")

        repoVersionFile = publishArea + 'repository'
        if not os.path.isfile(repoVersionFile):
            with open(repoVersionFile, 'w') as f:
                f.write(_LAYOUT_VERSION)

    except KeyboardInterrupt:
        log.info(_format_str("Aborting...", "green"))
        if 'files2cleanup' in locals():
            for backup in files2cleanup:
                _cleanup(backup)
        if 'outFiles' in locals(): _cleanup(outFiles)
        log.info(_format_str("Aborted by user.", "green"))
        return 1

    # Update the 'current' file
    with open(publishArea + 'current', 'w') as f:
        f.write(currentDateTime)

    log.info(_format_str("Done!", "green"))

    # Delete .startPublishing file when completing with success
    if os.path.exists(startfilepath):
        os.remove(startfilepath)
        log.debug(
            _format_str(
                "Flow control switch file '%s' deleted." % startfilepath,
                "green"))
    return 0


if __name__ == '__main__':
    import sys
    sys.exit(main())

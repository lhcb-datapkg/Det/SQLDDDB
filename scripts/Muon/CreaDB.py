###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Script to automatic generate all the files included in the subdir Muon/GapDir
# @author: Alessia Satta
import os, sys


def insertStation(stat_num, f):
    f.write(str(stat_num))


#    f.write("\n")
#    print "station M",stat_num


def insertSide(side, f):
    f.write(side + "Side")


#    f.write("\n")
#    print "station M",stat_num


def insertRegion(side, f):
    f.write(str(side))


#    f.write("\n")
#    print "station M",stat_num


def insertCham(ch, f):
    f.write(str(ch))


#    f.write("\n")
#    print "station M",stat_num


def inserthName(ch, f):
    f.write(str(ch))


#    f.write("\n")
#    print "station M",stat_num


def insertGap(ch, f):
    f.write(str(ch))


#    f.write("\n")
#    print "station M",stat_num


def insertGrid(i, j, f):
    if i == 1:
        val = gridTypeM1[j - 1]
    elif i == 2:
        val = gridTypeM2[j - 1]
    elif i == 3:
        val = gridTypeM3[j - 1]
    elif i == 4:
        val = gridTypeM4[j - 1]
    elif i == 5:
        val = gridTypeM5[j - 1]
    f.write("G")
    f.write(str(val))


def chamRegSide(i, j):
    if i == 1 and j == "A":
        tipo = chaR1ASide
    elif i == 1 and j == "C":
        tipo = chaR1CSide
    elif i == 2 and j == "A":
        tipo = chaR2ASide
    elif i == 2 and j == "C":
        tipo = chaR2CSide
    elif i == 3 and j == "A":
        tipo = chaR3ASide
    elif i == 3 and j == "C":
        tipo = chaR3CSide
    elif i == 4 and j == "A":
        tipo = chaR4ASide
    elif i == 4 and j == "C":
        tipo = chaR4CSide

    return tipo


def hNamRegSide(i, j):
    if i == 1 and j == "A":
        nome = hNamR1ASide
    elif i == 1 and j == "C":
        nome = hNamR1CSide
    elif i == 2 and j == "A":
        nome = hNamR2ASide
    elif i == 2 and j == "C":
        nome = hNamR2CSide
    elif i == 3 and j == "A":
        nome = hNamR3ASide
    elif i == 3 and j == "C":
        nome = hNamR3CSide
    elif i == 4 and j == "A":
        nome = hNamR4ASide
    elif i == 4 and j == "C":
        nome = hNamR4CSide

    return nome


def gapsinStation(i):
    if i == 1:
        ng = gapsM1
    elif i == 2:
        ng = gapsM2
    elif i == 3:
        ng = gapsM3
    elif i == 4:
        ng = gapsM4
    elif i == 5:
        ng = gapsM5
    return ng


stations = range(1, 6)
side = ['A', 'C']
regions = range(1, 5)
gapsM1 = range(1, 3)
gapsM2 = range(1, 5)
gapsM3 = range(1, 5)
gapsM4 = range(1, 5)
gapsM5 = range(1, 5)

#gridTypeM1=[1, 11,8, 10]
#gridTypeM2=["2", "4","12", "13"]
#gridTypeM3=["2", "4","12", "13"]
#gridTypeM4=["6", "7","9", "14"]
#gridTypeM5=["6", "7","9", "14"]
gridTypeM1 = [1, 11, 8, 10]
gridTypeM2 = [2, 4, 12, 13]
gridTypeM3 = [3, 5, 12, 13]
gridTypeM4 = [6, 7, 9, 14]
gridTypeM5 = [6, 7, 9, 14]

chaR1ASide = ["001", "002", "005", "007", "009", "010"]
chaR1CSide = ["003", "004", "006", "008", "011", "012"]

chaR2ASide = [
    "001", "002", "005", "006", "009", "011", "013", "015", "017", "018",
    "021", "022"
]
chaR2CSide = [
    "003", "004", "007", "008", "010", "012", "014", "016", "019", "020",
    "023", "024"
]

chaR3ASide = [
    "001", "002", "005", "006", "009", "010", "013", "014", "017", "019",
    "021", "023", "025", "027", "029", "031", "033", "034", "037", "038",
    "041", "042", "045", "046"
]

chaR3CSide = [
    "003", "004", "007", "008", "011", "012", "015", "016", "018", "020",
    "022", "024", "026", "028", "030", "032", "035", "036", "039", "040",
    "043", "044", "047", "048"
]

chaR4ASide = [
    "001", "002", "003", "004", "009", "010", "011", "012", "017", "018",
    "019", "020", "025", "026", "027", "028", "033", "034", "035", "036",
    "041", "042", "043", "044", "049", "050", "051", "052", "057", "058",
    "059", "060", "065", "066", "069", "070", "073", "074", "077", "078",
    "081", "082", "085", "086", "089", "090", "093", "094", "097", "098",
    "101", "102", "105", "106", "109", "110", "113", "114", "117", "118",
    "121", "122", "125", "126", "129", "130", "131", "132", "137", "138",
    "139", "140", "145", "146", "147", "148", "153", "154", "155", "156",
    "161", "162", "163", "164", "169", "170", "171", "172", "177", "178",
    "179", "180", "185", "186", "187", "188"
]

chaR4CSide = [
    "005", "006", "007", "008", "013", "014", "015", "016", "021", "022",
    "023", "024", "029", "030", "031", "032", "037", "038", "039", "040",
    "045", "046", "047", "048", "053", "054", "055", "056", "061", "062",
    "063", "064", "067", "068", "071", "072", "075", "076", "079", "080",
    "083", "084", "087", "088", "091", "092", "095", "096", "099", "100",
    "103", "104", "107", "108", "111", "112", "115", "116", "119", "120",
    "123", "124", "127", "128", "133", "134", "135", "136", "141", "142",
    "143", "144", "149", "150", "151", "152", "157", "158", "159", "160",
    "165", "166", "167", "168", "173", "174", "175", "176", "181", "182",
    "183", "184", "189", "190", "191", "192"
]

hNamR1ASide = ["18A2", "18A1", "17A2", "16A2", "15A2", "15A1"]

hNamR1CSide = ["18A1", "18A2", "17A2", "16A2", "15A1", "15A2"]

hNamR2ASide = [
    "20A3", "20A1", "19A3", "19A1", "18A3", "17A3", "16A3", "15A3", "14A3",
    "14A1", "13A3", "13A1"
]

hNamR2CSide = [
    "20A1", "20A3", "19A1", "19A3", "18A3", "17A3", "16A3", "15A3", "14A1",
    "14A3", "13A1", "13A3"
]

hNamR3ASide = [
    "24B", "24A", "23B", "23A", "22B", "22A", "21B", "21A", "20B", "19B",
    "18B", "17B", "16B", "15B", "14B", "13B", "12B", "12A", "11B", "11A",
    "10B", "10A", " 9B", " 9A"
]

hNamR3CSide = [
    "24A", "24B", "23A", "23B", "22A", "22B", "21A", "21B", "20B", "19B",
    "18B", "17B", "16B", "15B", "14B", "13B", "12A", "12B", "11A", "11B",
    "10A", "10B", " 9A", " 9B"
]

hNamR4ASide = [
    "32D", "32C", "32B", "32A", "31D", "31C", "31B", "31A", "30D", "30C",
    "30B", "30A", "29D", "29C", "29B", "29A", "28D", "28C", "28B", "28A",
    "27D", "27C", "27B", "27A", "26D", "26C", "26B", "26A", "25D", "25C",
    "25B", "25A", "24D", "24C", "23D", "23C", "22D", "22C", "21D", "21C",
    "20D", "20C", "19D", "19C", "18D", "18C", "17D", "17C", "16D", "16C",
    "15D", "15C", "14D", "14C", "13D", "13C", "12D", "12C", "11D", "11C",
    "10D", "10C", " 9D", " 9C", " 8D", " 8C", " 8B", " 8A", " 7D", " 7C",
    " 7B", " 7A", " 6D", " 6C", " 6B", " 6A", " 5D", " 5C", " 5B", " 5A",
    " 4D", " 4C", " 4B", " 4A", " 3D", " 3C", " 3B", " 3A", " 2D", " 2C",
    " 2B", " 2A", " 1D", " 1C", " 1B", " 1A"
]

hNamR4CSide = [
    "32A", "32B", "32C", "32D", "31A", "31B", "31C", "31D", "30A", "30B",
    "30C", "30D", "29A", "29B", "29C", "29D", "28A", "28B", "28C", "28D",
    "27A", "27B", "27C", "27D", "26A", "26B", "26C", "26D", "25A", "25B",
    "25C", "25D", "24C", "24D", "23C", "23D", "22C", "22D", "21C", "21D",
    "20C", "20D", "19C", "19D", "18C", "18D", "17C", "17D", "16C", "16D",
    "15C", "15D", "14C", "14D", "13C", "13D", "12C", "12D", "11C", "11D",
    "10C", "10D", " 9C", " 9D", " 8A", " 8B", " 8C", " 8D", " 7A", " 7B",
    " 7C", " 7D", " 6A", " 6B", " 6C", " 6D", " 5A", " 5B", " 5C", " 5D",
    " 4A", " 4B", " 4C", " 4D", " 3A", " 3B", " 3C", " 3D", " 2A", " 2B",
    " 2C", " 2D", " 1A", " 1B", " 1C", " 1D"
]

#create the structure file "

mydir = "../../DDDB/Muon"
namefile = mydir + "/structure.xml"
f = open(namefile, 'w')
f.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n")
f.write("<!DOCTYPE DDDB SYSTEM \"../DTD/structure.dtd\" []>\n")
f.write("<DDDB>\n")

f.write(" <!-- File Generated with python script --> \n")
f.write("  <detelem name=\"Muon\" classID=\"11009\"> \n")
f.write(
    "    <geometryinfo lvname=\"/dd/Geometry/DownstreamRegion/Muon/lvMuon\" \n"
)
f.write(
    "                  condition =\"/dd/Conditions/Alignment/Muon/MuonSystem\" \n"
)
f.write("                  npath=\"MuonSubsystem\" \n")
f.write(
    "                  support=\"/dd/Structure/LHCb/DownstreamRegion\"/> \n")

for i in stations:
    f.write("    <detelemref href =\"stations.xml#M")
    insertStation(i, f)
    f.write("\" /> \n")

f.write("   </detelem>\n")
f.write("</DDDB>")
f.close()

#create stations.xml

namefile = mydir + "/stations.xml"
f = open(namefile, 'w')
f.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n")
f.write(" <!DOCTYPE DDDB SYSTEM \"../DTD/structure.dtd\" []> \n")
f.write(" <DDDB> \n")
for i in stations:
    f.write("  <detelem name =\"M")
    insertStation(i, f)
    f.write("\" > \n")
    f.write(
        "      <geometryinfo lvname=\"/dd/Geometry/DownstreamRegion/Muon/StatPos/lvM"
    )
    insertStation(i, f)
    f.write("\" \n")
    # insert the alignment conditions for the whole station.
    f.write("                    condition =\"/dd/Conditions/Alignment/Muon/M")
    insertStation(i, f)
    f.write("Station\" \n")
    #
    f.write("                    npath=\"pvM")
    if i > 1:
        f.write("uonBack/pvM")
    insertStation(i, f)
    f.write("\"  \n")
    f.write(
        "                    support=\"/dd/Structure/LHCb/DownstreamRegion/Muon\"/> \n"
    )
    for j in side:
        f.write("      <detelemref href =\"#M")
        insertStation(i, f)
        insertSide(j, f)
        f.write("\"/> \n")
    f.write("  </detelem> \n")
    for j in side:
        f.write("  <detelem name = \"M")
        insertStation(i, f)
        insertSide(j, f)
        f.write("\" > \n")
        f.write(
            " <geometryinfo lvname = \"/dd/Geometry/DownstreamRegion/Muon/StatPos/lvM"
        )
        insertStation(i, f)
        insertSide(j, f)
        f.write("\" \n")
        f.write(
            "                 support = \"/dd/Structure/LHCb/DownstreamRegion/Muon/M"
        )
        insertStation(i, f)
        f.write("\" \n")
        # insert the alignment conditions for half stations
        f.write(
            "                   condition =\"/dd/Conditions/Alignment/Muon/M")
        insertStation(i, f)
        insertSide(j, f)
        f.write("\" \n")
        #
        f.write("                   npath = \"pvM")
        insertStation(i, f)
        insertSide(j, f)
        f.write("\" /> \n")
        for r in regions:
            f.write("    <detelemref href =\"GapDir/M")
            insertStation(i, f)
            f.write("R")
            insertRegion(r, f)
            f.write("@RegStructure.xml#R")
            insertRegion(r, f)
            insertSide(j, f)
            f.write("\" /> \n")
        f.write("   </detelem> \n")
f.write("</DDDB>")
f.close()

#now the GapDir/MiRj@RegStructure.xml
dirname = mydir + "/GapDir"
print dirname
if not os.path.isdir(dirname):
    os.mkdir(dirname)
for i in stations:
    for j in regions:
        namefile = mydir + "/GapDir/M" + str(i) + "R" + str(
            j) + "@RegStructure.xml"
        f = open(namefile, 'w')
        f.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n")
        f.write(" <!DOCTYPE DDDB SYSTEM \"../../DTD/structure.dtd\" []> \n")
        f.write(" <DDDB> \n")
        for m in side:
            f.write(" <detelem name = \"R")
            insertRegion(j, f)
            insertSide(m, f)
            f.write("\" classID=\"11005\"> \n")
            f.write(
                "  <geometryinfo lvname = \"/dd/Geometry/DownstreamRegion/Muon/M"
            )
            insertStation(i, f)
            f.write("ChamPos/lvR")
            insertRegion(j, f)
            insertSide(m, f)
            f.write("\" \n")
            f.write(
                "     support = \"/dd/Structure/LHCb/DownstreamRegion/Muon/M")
            insertStation(i, f)
            f.write("/M")
            insertStation(i, f)
            insertSide(m, f)
            f.write("\" \n")
            f.write("     npath = \"pvM")
            insertStation(i, f)
            f.write("R")
            insertRegion(j, f)
            insertSide(m, f)
            f.write("\"/> \n")
            ##             if  j==1 and m=="A" :
            ##                 tipo=chaR1ASide
            ##             elif j==1 and m=="C":
            ##                 tipo=chaR1CSide
            ##             else:
            ##                  tipo=chaR1CSide
            tipo = chamRegSide(j, m)
            for c in tipo:
                f.write("   <detelemref href =\"#Cham")
                insertCham(c, f)
                f.write("\" /> \n")
            f.write("  </detelem> \n")
            f.write("\n")
#insert grid  for each chamber :-(
        for m in side:
            tipo = chamRegSide(j, m)
            nome = hNamRegSide(j, m)
            for kk in range(len(tipo)):
                c = tipo[kk]
                Hn = nome[kk]
                f.write("  <detelem name = \"Cham")
                insertCham(c, f)
                f.write("\" classID=\"11006\"> \n")
                f.write("    <userParameter name=\"Grid\" type= \"string\"> ")
                insertGrid(i, j, f)
                f.write(" </userParameter> \n")
                # here put some function to set the hardware name
                f.write("    <userParameter name=\"hName\" type= \"string\"> ")
                inserthName(Hn, f)
                f.write(" </userParameter>  \n")
                # here put some function to set the mask name
                f.write("    <conditioninfo name=\"Mask")
                insertCham(c, f)
                f.write(
                    "\" condition=\"/dd/Conditions/ReadoutConf/Muon/Masks/M")
                insertStation(i, f)
                f.write("/R")
                insertRegion(j, f)
                f.write("/Mask")
                insertCham(c, f)
                f.write("\" /> \n")
                f.write("    <conditioninfo name=\"")
                insertGrid(i, j, f)
                f.write("\" condition=\"/dd/Conditions/ReadoutConf/Muon/Grid/")
                insertGrid(i, j, f)
                f.write("\" /> \n")
                f.write(
                    "    <geometryinfo lvname  = \"/dd/Geometry/DownstreamRegion/Muon/M"
                )
                insertStation(i, f)
                f.write("R")
                insertRegion(j, f)
                f.write("Cham/lvM")
                insertStation(i, f)
                f.write("R")
                insertRegion(j, f)
                f.write("Cham\" \n")
                f.write(
                    "                    support = \"/dd/Structure/LHCb/DownstreamRegion/Muon/M"
                )
                insertStation(i, f)
                f.write("/M")
                insertStation(i, f)
                insertSide(m, f)
                f.write("/R")
                insertRegion(j, f)
                insertSide(m, f)
                f.write("\"\n")
                # insert here the alignment condition
                f.write(
                    "                    condition =\"/dd/Conditions/Alignment/Muon/M"
                )
                insertStation(i, f)
                f.write("/R")
                insertRegion(j, f)
                f.write("/Cham")
                insertCham(c, f)
                f.write("\"\n")
                f.write("                    npath   = \"pvM")
                insertStation(i, f)
                f.write("R")
                insertRegion(j, f)
                f.write("Cham")
                insertCham(c, f)
                f.write("\" /> \n")
                gaps = gapsinStation(i)
                ##                 for g in gaps:
                ##                     f.write("    <detelemref href =\"GapStructureM")
                ##                     insertStation(i,f)
                ##                     f.write("R")
                ##                     insertRegion(j,f)
                ##                     f.write("/GapStructureM")
                ##                     insertStation(i,f)
                ##                     f.write("R")
                ##                     insertRegion(j,f)
                ##                     f.write("Cham")
                ##                     insertCham(c,f)
                ##                     f.write(".xml#Gap")
                ##                     insertGap(g,f)
                ##                     f.write("\" /> \n")
                for g in gaps:
                    f.write("    <detelemref href =\"GapStructureM")
                    insertStation(i, f)
                    f.write("R")
                    insertRegion(j, f)
                    f.write("/Cham")
                    insertCham(c, f)
                    f.write("@GapStructureM")
                    insertStation(i, f)
                    f.write("R")
                    insertRegion(j, f)
                    #                    f.write("Cham")
                    #                    insertCham(c,f)
                    f.write(".xml#Gap")
                    insertGap(g, f)
                    f.write("\" /> \n")
                f.write("  </detelem> \n")
                f.write("\n")
        f.write("</DDDB>")
        f.close()

#now the GapDir/GapStructureMiRj/Cham00m@GapStructureMiRj.xml

for i in stations:
    for j in regions:

        dirname = mydir + "/GapDir/GapStructureM" + str(i) + "R" + str(j)
        print dirname
        if not os.path.isdir(dirname):
            #            print os.chdir(dirname)
            os.mkdir(dirname)
        for m in side:
            tipo = chamRegSide(j, m)
            for c in tipo:
                namefile1 = mydir + "/GapDir/GapStructureM" + str(
                    i) + "R" + str(j) + "/Cham" + str(c)
                namefile2 = "@GapStructureM" + str(i) + "R" + str(j) + ".xml"
                namefile = namefile1 + namefile2
                f = open(namefile, 'w')
                f.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?> \n")
                f.write(
                    " <!DOCTYPE DDDB SYSTEM \"../../../DTD/structure.dtd\" []> \n"
                )
                f.write(" <DDDB> \n")
                gaps = gapsinStation(i)
                for g in gaps:
                    f.write("<detelem name = \"Gap")
                    insertGap(g, f)
                    f.write("\" classID=\"11007\">  \n")
                    f.write(
                        "    <geometryinfo lvname = \"/dd/Geometry/DownstreamRegion/Muon/M"
                    )
                    insertStation(i, f)
                    f.write("R")
                    insertRegion(j, f)
                    f.write("Cham/lvGasGap\" \n")
                    f.write(
                        "       support = \"/dd/Structure/LHCb/DownstreamRegion/Muon/M"
                    )
                    insertStation(i, f)
                    f.write("/M")
                    insertStation(i, f)
                    insertSide(m, f)
                    f.write("/R")
                    insertRegion(j, f)
                    insertSide(m, f)
                    f.write("/")
                    f.write("Cham")
                    insertCham(c, f)
                    f.write("\" \n")
                    f.write("        npath = \"pvGap")
                    insertGap(g, f)
                    f.write("\" /> \n")
                    f.write("       <detelemref href =\"#GasGap")
                    insertGap(g, f)
                    f.write("\" /> \n")
                    f.write(" </detelem> \n")
                for g in gaps:
                    f.write("<detelem name = \"GasGap")
                    insertGap(g, f)
                    f.write("\" classID=\"11007\"> \n")
                    f.write(
                        "    <geometryinfo lvname = \"/dd/Geometry/DownstreamRegion/Muon/M"
                    )
                    insertStation(i, f)
                    f.write("R")
                    insertRegion(j, f)
                    f.write("Cham/lvGasGapLayer5\" \n")
                    f.write(
                        "       support = \"/dd/Structure/LHCb/DownstreamRegion/Muon/M"
                    )
                    insertStation(i, f)
                    f.write("/M")
                    insertStation(i, f)
                    insertSide(m, f)
                    f.write("/R")
                    insertRegion(j, f)
                    insertSide(m, f)
                    f.write("/")
                    f.write("Cham")
                    insertCham(c, f)
                    f.write("/Gap")
                    insertGap(g, f)
                    f.write("\" \n")
                    f.write("        npath = \"pvGapLayer5\" /> \n")
                    f.write("  </detelem> \n")
                f.write("\n")
                f.write("</DDDB>")
                f.close()

# R1ASide           |           R1CSide
#                   |
# 001 18A2 002 18A1 | 003 18A1 004 18A2
# 005 17A2          |          006 17A2
# 007 16A2          |          008 16A2
# 009 15A2 010 15A1 | 011 15A1 012 15A2
#
#
# R2ASide           |           R2CSide
#                   |
# 001 20A3 002 20A1 | 003 20A1 004 20A3
# 005 19A3 006 19A1 | 007 19A1 008 19A3
# 009 18A3          |          010 18A3
# 011 17A3          |          012 17A3
# 013 16A3          |          014 16A3
# 015 15A3          |          016 15A3
# 017 14A3 018 14A1 | 019 14A1 020 14A3
# 021 13A3 022 13A1 | 023 13A1 024 13A3
#
#
# R3ASide         |         R3CSide
#                 |
# 001 24B 002 24A | 003 24A 004 24B
# 005 23B 006 23A | 007 23A 008 23B
# 009 22B 010 22A | 011 22A 012 22B
# 013 21B 014 21A | 015 21A 016 21B
# 017 20B         |         018 20B
# 019 19B         |         020 19B
# 021 18B         |         022 18B
# 023 17B         |         024 17B
# 025 16B         |         026 16B
# 027 15B         |         028 15B
# 029 14B         |         030 14B
# 031 13B         |         032 13B
# 033 12B 034 12A | 035 12A 036 12B
# 037 11B 038 11A | 039 11A 040 11B
# 041 10B 042 10A | 043 10A 044 10B
# 045  9B 046  9A | 047  9A 048  9B
#
#
# R4ASide                         |                         R4CSide
#                                 |
# 001 32D 002 32C 003 32B 004 32A | 005 32A 006 32B 007 32C 008 32D
# 009 31D 010 31C 011 31B 012 31A | 013 31A 014 31B 015 31C 016 31D
# 017 30D 018 30C 019 30B 020 30A | 021 30A 022 30B 023 30C 024 30D
# 025 29D 026 29C 027 29B 028 29A | 029 29A 030 29B 031 29C 032 29D
# 033 28D 034 28C 035 28B 036 28A | 037 28A 038 28B 039 28C 040 28D
# 041 27D 042 27C 043 27B 044 27A | 045 27A 046 27B 047 27C 048 27D
# 049 26D 050 26C 051 26B 052 26A | 053 26A 054 26B 055 26C 056 26D
# 057 25D 058 25C 059 25B 060 25A | 061 25A 062 25B 063 25C 064 25D
# 065 24D 066 24C                 |                 067 24C 068 24D
# 069 23D 070 23C                 |                 071 23C 072 23D
# 073 22D 074 22C                 |                 075 22C 076 22D
# 077 21D 078 21C                 |                 079 21C 080 21D
# 081 20D 082 20C                 |                 083 20C 084 20D
# 085 19D 086 19C                 |                 087 19C 088 19D
# 089 18D 090 18C                 |                 091 18C 092 18D
# 093 17D 094 17C                 |                 095 17C 096 17D
# 097 16D 098 16C                 |                 099 16C 100 16D
# 101 15D 102 15C                 |                 103 15C 104 15D
# 105 14D 106 14C                 |                 107 14C 108 14D
# 109 13D 110 13C                 |                 111 13C 112 13D
# 113 12D 114 12C                 |                 115 12C 116 12D
# 117 11D 118 11C                 |                 119 11C 120 11D
# 121 10D 122 10C                 |                 123 10C 124 10D
# 125  9D 126  9C                 |                 127  9C 128  9D
# 129  8D 130  8C 131  8B 132  8A | 133  8A 134  8B 135  8C 136  8D
# 137  7D 138  7C 139  7B 140  7A | 141  7A 142  7B 143  7C 144  7D
# 145  6D 146  6C 147  6B 148  6A | 149  6A 150  6B 151  6C 152  6D
# 153  5D 154  5C 155  5B 156  5A | 157  5A 158  5B 159  5C 160  5D
# 161  4D 162  4C 163  4B 164  4A | 165  4A 166  4B 167  4C 168  4D
# 169  3D 170  3C 171  3B 172  3A | 173  3A 174  3B 175  3C 176  3D
# 177  2D 178  2C 179  2B 180  2A | 181  2A 182  2B 183  2C 184  2D
# 185  1D 186  1C 187  1B 188  1A | 189  1A 190  1B 191  1C 192  1D

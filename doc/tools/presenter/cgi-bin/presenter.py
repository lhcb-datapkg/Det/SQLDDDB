#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""This script extracts the partition global tags dependency trees from the CondDB Release Notes.

This script is intended to be used with a cgi interface to extract possible
global tags dependency tries from a release_notes.xml file of a Det/SQLDDDB package.
At a web page form the user have to specify the CondDB partition he wants to analyze
(could be DDDB, LHCBCOND or SIMCOND) and for SIMCOND chose also the Velo(Opened/Closed)
and Magnet (polarity:Up/Off/Down) configuration scheme.
The script has two regimes now:
1) It looks for the most recent one global tag in the partition, and
builds the dependency tree almost to the beginning of time ;), returning for the user
an html page with a result.
2) It also has the functionality to produce the list of all current top global tags for their
branches and the user can choose which global tag branch dependency sequence for the requested
partition he wants to see. The result is returned also as html page.

"""

__author__ = "Illya Shapoval"
__credits__ = "Illya Shapoval <illya.shapoval@cern.ch>, Marco Clemencic <marco.clemencic@cern.ch>"
__version__ = "$Id: presenter.py,v 1.1 2009-12-14 20:00:59 ishapova Exp $"
__maintainer__ = "Illya Shapoval <illya.shapoval@cern.ch>"

import os, sys
import cgi
import cgitb
cgitb.enable()

import xml.etree.ElementTree as ET

# define needed namespaces
ET._namespace_map["http://lhcb.cern.ch"] = "lhcb"
ET._namespace_map["http://www.w3.org/2001/XMLSchema-instance"] = "xsi"

_xel = lambda t: ET.QName("http://lhcb.cern.ch", t)


def get_last_GT_name(rootelement,
                     partition,
                     velo_magnet_state,
                     branch_type=None):
    """Finds and returns the most recent global tag name for given partition.

    'velo_magnet_state' variable is used only for SIMCOND partition and
    can be the sum of any following string pairs:
      'vc' - correponds to opened velo;
      'vo' - velo closed;
      '-mu100' - magnet polarity up;
      '-moff' - magnet off;
      '-md100' - magnet polarity down.
    """
    last_GT_name = ""
    for element in rootelement:
        if last_GT_name: break
        if element.tag == _xel("global_tag"):
            for subelement in element:
                if subelement.tag == _xel("partition"):
                    # Check if current global tag element corresponds to requested partition
                    if subelement.find(str(_xel("name"))).text == partition:
                        GT_name = element.find(str(_xel("tag"))).text
                        # Checking that requested velo and magnet configuration (valid only for SIMCOND)
                        # or branch type is met here
                        if partition == "SIMCOND" and velo_magnet_state:
                            if velo_magnet_state not in GT_name: continue
                        elif (partition in ["DDDB", "LHCBCOND"
                                            ]) and branch_type:
                            if branch_type not in GT_name: continue
                        last_GT_name = GT_name
                        return last_GT_name


def get_GT_element(rootelement, partition, GT_name):
    """Finds and returns the xml element of global tag.

     It looks for a global tag element with the name 'GT_name'
     for partition 'partition'.
     """
    for element in rootelement:
        if element.tag == _xel("global_tag"):
            for subelement1 in element:
                if subelement1.tag == _xel(
                        "tag") and subelement1.text == GT_name:
                    # Before returning the global tag element checking partition to be requested.
                    for subelement2 in element:
                        if subelement2.tag == _xel("partition"):
                            for part_subelement2 in subelement2:
                                if part_subelement2.tag == _xel(
                                        "name"
                                ) and part_subelement2.text == partition:
                                    return element


def get_base_and_LTs(partition, GT_element):
    """Finds properties of the chosen global tag element (GT_element) for requested partition.

    For the given partition name (partition) returns the base
    global tag name (base_GT_name) and the list of local
    tags (local_tag_names) of the given global tag element (GT_element) .
    """

    local_tag_names = []
    found_needed_partition = False  # Flag to indicate that the element with needed partition was entered
    base_GT_name = ""
    for GT_subelement in GT_element:
        if found_needed_partition:
            break  # When requested partition in the found global tag element is found - return collected 'garbage' :)
        if GT_subelement.tag == _xel("partition"):
            for part_subelement in GT_subelement:
                if part_subelement.tag == _xel(
                        "name") and part_subelement.text == partition:
                    found_needed_partition = True
                    continue
                if found_needed_partition and part_subelement.tag == _xel(
                        "base"):
                    base_GT_name = part_subelement.text
                    continue
                if found_needed_partition and part_subelement.tag == _xel(
                        "tag"):
                    local_tag_names.append(part_subelement.text)
    return base_GT_name, local_tag_names


def get_all_GTs(rootelement, partition):
    """The function finds all global tags ever created for a partition."""
    all_gts = []
    for element in rootelement:
        if element.tag == _xel("global_tag"):
            # Before returning the global tag element checking partition to be requested.
            for subelement2 in element:
                if subelement2.tag == _xel("partition"):
                    for part_subelement2 in subelement2:
                        if part_subelement2.tag == _xel(
                                "name") and part_subelement2.text == partition:
                            for subelement1 in element:
                                if subelement1.tag == _xel("tag"):
                                    all_gts.append(subelement1.text)
    return all_gts


def get_branches_hats(rootelement, partition, velo_magnet_state):
    """The function finds the latest global tag for every branch for a partition

    For doing that it compares the list of all global tags ever created for a partition
    and the list of all global tags, which were used as base global tags.
    """
    all_base_gts = []
    all_gts_per_partition = get_all_GTs(rootelement, partition)
    for element in rootelement:
        if element.tag == _xel("global_tag"):
            # Before returning the global tag element checking partition to be requested.
            for subelement1 in element:
                if subelement1.tag == _xel("partition"):
                    for part_subelement in subelement1:
                        if part_subelement.tag == _xel(
                                "name") and part_subelement.text == partition:
                            for part_subelement1 in subelement1:
                                if part_subelement1.tag == _xel("base"):
                                    if (part_subelement1.text in
                                            all_gts_per_partition) and (
                                                part_subelement1 not in
                                                all_base_gts):
                                        all_base_gts.append(
                                            part_subelement1.text)
    branches_hats = list(set(all_gts_per_partition) - set(all_base_gts))
    if velo_magnet_state:
        filtered_branches_hats = []
        for tag in branches_hats:
            if velo_magnet_state in tag:
                filtered_branches_hats.append(tag)
        branches_hats = filtered_branches_hats
    return branches_hats


def get_descriptions(rootelement, partition, all_LTs):
    """Collects the descriptions for every local tag in 'all_LTs' list for requested partition.

    'all_LTs' - list of all local tag names, included in the very recent one global tag
    """
    all_LTs_descriptions = {}
    all_LTs_1D = [
    ]  # one dimensional list of all local tags, which belong to requested global tag
    for i in all_LTs:
        all_LTs_1D += i

    for element in rootelement:
        if element.tag == _xel("note"):
            part_elements = element.findall(str(_xel("partition")))
            for part_element in part_elements:
                tag_element = part_element.find(str(_xel("tag")))
                name_element = part_element.find(str(_xel("name")))

                if tag_element.text in all_LTs_1D:
                    if partition == name_element.text:
                        desc_element = element.find(str(_xel("description")))
                        full_desc = ""
                        # This loop is for collecting of all the lines of the local tag description
                        for subsubelement in desc_element:
                            if subsubelement.tag == _xel("di"):
                                full_desc += "\n" + subsubelement.text
                        all_LTs_descriptions[tag_element.text] = full_desc
                        full_desc = ""

    return all_LTs_descriptions


def display_dependency_tree(rootelement, partition, GT_name, search_mode=None):
    """The function builds and displays the dependency sequence for GT_name and partition."""
    found_GT_element = True
    all_GTs = [GT_name]
    all_LTs = []
    while found_GT_element:
        # Find the global tag element for given partition and global tag name (GT_name)
        found_GT_element = get_GT_element(rootelement, partition, GT_name)
        if not found_GT_element: break
        # Find base global tag name and its local tags included in found global tag element
        base_GT_name, local_tag_names = get_base_and_LTs(
            partition, found_GT_element)
        all_GTs.append(base_GT_name)
        all_LTs.append(local_tag_names)
        GT_name = base_GT_name
    all_LTs_descriptions = get_descriptions(rootelement, partition, all_LTs)

    # Create an object of empty .xhtml page (tree), which has only general content
    root = initialize_XHTML(partition, search_mode)

    # Make the xhtml entries one by one for global tag "gt" and its list of included local tags "lt"
    for gt, lt in zip(all_GTs, all_LTs):
        root = make_XHTML_entry(root, gt, lt, all_LTs_descriptions)

    # Write the .xhtml tree object to file
    finilize_XHTML(root)


def display_branches_hats(rootelement, partition, branches_hats, search_mode,
                          velo_magnet_state):
    """The function builds and displays the top global tags for every present branch for a partition."""
    branches_hats.sort()
    branches_hats.reverse()
    latest_GT_names = []
    velo_magnet_states = [velo_magnet_state]
    # Collect the latest global tag names for definite branches (for definite symbols in GT names)
    if partition == "SIMCOND":
        if not velo_magnet_state:
            # If for SIMCOND 'all configurations' are activated for branch view
            velo_magnet_states = [
                "vc-md", "vc-moff", "vc-mu", "vo-md", "vo-moff", "vo-mu",
                "vc15mm-md"
            ]
        for state in velo_magnet_states:
            latest_GT_names.append(
                get_last_GT_name(rootelement, partition, state))
    else:
        branch_types = ["hlt", "head"]
        for type in branch_types:
            latest_GT_names.append(
                get_last_GT_name(rootelement, partition, velo_magnet_states[0],
                                 type))

    # Buid the xhtml page
    root = initialize_XHTML(partition, search_mode, velo_magnet_state)
    for hat in branches_hats:
        root = make_XHTML_hat_entry(root, hat, latest_GT_names)
    finilize_XHTML(root)


def initialize_XHTML(partition, search_mode, velo_magnet_state=None):
    """Prepares the common part of the xhtml page. Adds partition name, buttons and some explanations."""

    #### The HEAD for html ###############################################
    root = ET.Element("html")
    head = ET.SubElement(root, "head")

    title = ET.SubElement(head, "title")
    title.text = "%s partition" % partition
    script = ET.SubElement(head, "script", {"type": "text/javascript"})
    script.text = """
function toggle(link_id, descr_id)
{
  var linkText = document.getElementById(link_id).innerHTML;
  //alert(link_id);
  if(linkText == "[+]")
  {
    document.getElementById(link_id).innerHTML = "[-]";
    document.getElementById(descr_id).style.display = "block";
  }
  else
  {
    //alert(linkText);
    document.getElementById(link_id).innerHTML = "[+]";
    document.getElementById(descr_id).style.display = "none";
  }
}
"""
    body = ET.SubElement(root, "body", {"style": "background-color:#C8C8DA"})

    ########## The title of a page ######################################
    title_table = ET.SubElement(body, "table", {
        "border": "1",
        "bgcolor": "#5858AF",
        "width": "100%"
    })
    title_row = ET.SubElement(title_table, "tr")
    title_cell = ET.SubElement(title_row, "td")
    title_center = ET.SubElement(title_cell, "center")
    name_head = ET.SubElement(title_center, "h1")
    name_head.text = "CondDB Partition"

    ########## Navigation Main Menu buttons ##############################
    menu_button = ET.SubElement(body, "button", {"type":"button", "name":"Branches", \
                                                 "onclick":'javascript: location.assign("../conddb/form.html")',\
                                               "style":"color: #006400; "})
    menu_button.text = "Main menu"
    align = ET.SubElement(body, "center")
    name = ET.SubElement(align, "h1")
    name.text = partition
    body.insert(3, menu_button)

    ########## Building the branches hat view ##############################
    if search_mode == "from_branches":
        form = ET.SubElement(align, "form", {
            "method": "get",
            "action": "../cgi-bin/presenter.cgi",
            "name": "hats"
        })
        if not velo_magnet_state:
            form.text = "This global tags are the hats of all %s global tag branches. Click a branch of interest:" \
            %partition
        else:
            form.text = "This global tags are the hats of %s global tag branches for '%s' configuration. Click a branch of interest:"\
             %(partition, velo_magnet_state)
        breakline1 = ET.SubElement(form, "br")
        breakline2 = ET.SubElement(form, "br")
        partition = ET.SubElement(form, "input", {
            "type": "hidden",
            "name": "partition",
            "value": partition
        })
    # Building the dependency sequence view, entered from branches view ####
    elif not search_mode:
        back_button = ET.Element("button", {"type":"button", "name":"Back", \
                                                 "onclick":"javascript: history.go(-1)",\
                                               "style":"color: #006400; "})
        back_button.text = "Branches"
        body.insert(2, back_button)
        body.insert(5, back_button)
    return root


def make_XHTML_entry(root, GT_name, localTags, all_LTs_descriptions):
    """Creates the xhtml entry for every global tag with its local tags and descriptions."""
    body = root.find("body")
    center = body.find("center")
    gtagLine = ET.SubElement(center, "b", {"style": "color: #800000; "})
    gtag = ET.SubElement(gtagLine, "big")
    gtag.text = GT_name

    for lt in localTags:
        table = ET.SubElement(center, "table", {"border":"1", "cellspacing":"0", \
                                                 "style":"background: #5858AF; color: #FFFFFF; "})
        row = ET.SubElement(table, "tr")
        cell = ET.SubElement(row, "td", {"width": "170", "align": "center"})
        cell.text = lt
        linkCell = ET.SubElement(row, "td", {"width": "25"})
        clickable = ET.SubElement(linkCell, "a", {"title":"show/hide description", "id":"%s_link" %lt,\
                                               "href":"javascript: void(0);", \
                                               "onclick":'toggle("%s_link", "%s");' %(lt,lt),\
                                                "style":"text-decoration: none; color: #00BFFF; text-align:right"})
        clickable.text = "[-]"

        hiddenCell = ET.SubElement(row, "td", {"id": lt, "align": "left"})
        ltagLine = ET.SubElement(hiddenCell, "i")
        ltagLine.text = all_LTs_descriptions[lt]

        script = ET.SubElement(center, "script", {"type": "text/javascript"})
        script.text = 'toggle("%s_link","%s");' % (lt, lt)
    arrow = ET.SubElement(center, "p")
    arrow.text = "/|\\"

    return root


def make_XHTML_hat_entry(root, branch_hat_name, latest_GT_names):
    """Creates the xhtml entry for every global tag branch hat."""
    body = root.find("body")
    center = body.find("center")
    form = center.find("form")

    if branch_hat_name not in latest_GT_names:
        button = ET.SubElement(form, "input", {
            "type": "submit",
            "name": "branch_hat",
            "value": branch_hat_name
        })
    else:
        button = ET.SubElement(form, "input", {"type":"submit", "name":"branch_hat", "value":branch_hat_name, \
                                               "style":"color: #0000FF; "})
    break_line = ET.SubElement(form, "br")
    break_line2 = ET.SubElement(form, "br")

    return root


def finilize_XHTML(root):
    """Writing out the xhtml tree object to the stdout, prepending it with a line for cgi processing."""
    tree = ET.ElementTree(root)
    print "content-type: text/html\n"
    print '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">'
    tree.write(sys.stdout)

    # For the case of file write out
    #file = open("depend.html","w")
    #tree.write(file)
    #file.close()


def main():
    FormData = cgi.FieldStorage()

    ##########################################################################################
    # Web page form variables manipulations and partition verification
    ##########################################################################################

    partition = FormData["partition"].value
    partitions = ["DDDB", "LHCBCOND", "SIMCOND"]
    if partition not in partitions:
        print "'%s' is not a valid partition name. Allowed: %s" % (partition,
                                                                   partitions)
        return 1
    #################################
    if FormData.has_key("search_mode"):
        search_mode = FormData["search_mode"].value
        #################################
        if partition == "SIMCOND":
            if search_mode == "from_latest":
                velo_magnet_state = FormData["velo_state"].value + FormData[
                    "magnet_state"].value
            elif search_mode == "from_branches":
                if not FormData.has_key("all_config"):
                    velo_magnet_state = FormData[
                        "velo_state"].value + FormData["magnet_state"].value
                    #all_configurations = None
                elif FormData.has_key("all_config"):
                    velo_magnet_state = None
                    #all_configurations = FormData["all_config"]
        else:
            #all_configurations = None
            velo_magnet_state = None
    else:
        branch_hat = FormData["branch_hat"].value
    ##########################################################################################
    # Opening the release notes xml tree
    ##########################################################################################

    from CondDBUI.Admin import ReleaseNotes
    #import ReleaseNotes
    try:
        rn = ReleaseNotes("../conddb/release_notes.xml")
    except IOError:
        print "\nSorry.. Path to Release Notes file is not valid or hardware IO problems occurred.\
               \nCheck path or try again later."

        return 1

    rootelement = rn.tree.getroot()

    ##########################################################################################
    # Parsing the release notes tree
    ##########################################################################################
    if FormData.has_key("search_mode"):
        # Build dependency sequence for latest global tag for the given partition
        if search_mode == "from_latest":
            last_gt_name = get_last_GT_name(rootelement, partition,
                                            velo_magnet_state)
            display_dependency_tree(rootelement, partition, last_gt_name,
                                    search_mode)
        # Build dependency sequence for particular global tag branch for the given partition
        elif search_mode == "from_branches":
            branches_hats = get_branches_hats(rootelement, partition,
                                              velo_magnet_state)
            display_branches_hats(rootelement, partition, branches_hats,
                                  search_mode, velo_magnet_state)
    else:
        display_dependency_tree(rootelement, partition, branch_hat)


if __name__ == '__main__':
    sys.exit(main())

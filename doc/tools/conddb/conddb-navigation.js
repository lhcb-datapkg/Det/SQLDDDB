/*! CondDB Release Notes Navigation functions */

/*Browser detection patch*/
jQuery.browser = {};
jQuery.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase());
jQuery.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
jQuery.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
jQuery.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());

$(document).scroll(function () {
    var $css3menu = $("#css3menu0");

    if( $(window).scrollTop() > 0 ){ $css3menu.stop().animate({ "opacity": 0.2 }, {duration:800, queue:false}); }
    else { $css3menu.stop().animate({ "top": 0, "opacity": 1 }, {duration:800, queue:false}); }
});

$(document).ready(function(){

    // Control the css3menu transparency
    var $css3menu = $("#css3menu0");
    $css3menu.mouseover(function () {
        $css3menu
        //.stop()
        .animate({"opacity": 1 });
    });
    $css3menu.mouseleave(function () {
        if($(window).scrollTop() != 0) {
            $css3menu
            //.stop()
            .animate({"opacity": 0.6 }); }
    });

    // Add scrolling the page to a local tag entry functionality
    $("span.addedtag, span.basetag").click(function(){
        var elem = $(this);
        var clickedPartName = elem.parent().find("span.partition").text();
        var clickedTagName = elem.text();
        var destClass = elem.hasClass("addedtag")?"tag":"globaltag";
        scrollToTag(clickedTagName, clickedPartName, destClass);
      });

    // Prepare to override a standard tip and pop-up a fancy one ('TipTip' plug-in)
    $("li.summary").mouseenter(function(){
        $(this).children("span.addedtag").each(function(){
          var elem = $(this);
          var desc = getDescription(elem.parent().find("span.partition").text(), elem.text());
          elem.tipTip({content: desc});
        });
      });

    // Add highlighting an added local tag
    $("span.addedtag").mouseenter(function(){
        $(this).effect("highlight", {color: "#7B68EE"}, 500);
      });

    // Add toggling the patch list of files
    $("span.fake-link").click(function() {
        var link_elem = $(this);
        var idnum = link_elem.attr("id").replace("link-","#");
        link_elem.text(link_elem.text() == 'show files' ? 'hide files' : 'show files');
        $(idnum).slideToggle("slow");
      });

});

function openCredits () {
    var $contacts = $("p.maintainer");

    //Prepare the contacts dialog
    $contacts.dialog({ width: 400,
                       show: { effect:"fade", duration:800},
                       hide: { effect:"fade", duration:1000},
                       modal: true,
                       autoOpen: false,
                       resizable: false});
    // Fix the dialog when scrolling page and open dialog
    $contacts.parent().css({position:"fixed"}).end().dialog('open');
};

function openDetectorTypesDialog(partition, tag_class) {
    var $dtypesCanvas = $("#dtypesCanvas");
    var $types_list = $("ul", $dtypesCanvas);
    $types_list.empty();
    var detector_types = getAllDetectorTypes(partition, tag_class);

    for (k = 0; k < detector_types.length; k++) {
    	var dt = detector_types[k];
    	var dweight = getAllTags(partition, dt, tag_class).length;
        $types_list.append("<li class='li_dtype_link'>" +
        		               "<a class='dtype_link' " +
        		                  "href='javaScript:void(0);' " +
        		                  "onclick=\'openTagsDialog(\"" + partition + "\",\"" + dt + "\",\"" + tag_class + "\");\' " +
        				          "data-weight='" + dweight + "'>" + dt +
        				       "</a></li>");
    };

    if( ! $dtypesCanvas.tagcanvas({
        textColour : '#FFFF00',
        outlineThickness : 1,
        outlineOffset : 20,
        pulsateTo : 0.2,
        pulsateTime : 0.75,
        maxSpeed : 0.05,
        reverse: true,
        weight: true,
        weightMode: 'size',
        weightFrom: 'data-weight',
        weightSizeMin: 15,
        weightSizeMax: 35,
        freezeActive: true,
        freezeDecel: true
      })) {
        // TagCanvas failed to load
        $dtypesCanvas.hide();
      };

    //Prepare the detector types dialog
    $dtypesCanvas.dialog({
        title: partition + " detector types (weighted by evolution) ",
        show: { effect:"fade", duration:800},
        hide: { effect:"fade", duration:1000},
        modal: true,
        height: 500,
        width: 550,
        autoOpen: false,
        resizable: false});

    // Fix the dialog when scrolling page
    $dtypesCanvas.parent().css({position:"fixed"}).end().dialog('open');
};

function openTagsDialog(partition, detector_type, tag_class) {
	var $tagsCanvas = $("#tagsCanvas");
    var $tags_list = $("ul", $tagsCanvas);
    var ctx = $("#base_canvas").get()[0].getContext('2d');

	// Clear the canvas
    ctx.clearRect(0, 0, $tagsCanvas.width(), $tagsCanvas.height());

	// Dialog geometry parameters
	var dialWidth = 500;
	var dialHeight = 550;

    // Fill the list of tags
    $tags_list.empty();
    var tags = getAllTags(partition, detector_type, tag_class);

    for (k = 0; k < tags.length; k++) {
        $tags_list.append("<li class='li_tag_link'>" +
                             "<a class='tag_link' " +
                                "href='javaScript:void(0);' " +
                                "data-weight='" + k + "' " +
                                "onclick='scrollToTag(\"" + tags[k] + "\",\"" + partition + "\",\"" + tag_class + "\");'>" + tags[k] +
                             "</a></li>");
    };
    // One empty tag to break the carousel ring
    $tags_list.append("<li><a href='javaScript:void(0);' onclick='return false;' data-weight='0'> </a></li>");

    // Draw the color-scheme explanation on the underlying canvas
    img = new Image();
    img.onload = function(){
        ctx.drawImage(img, 0, 0, 100, 40);
    };
    img.src = 'tools/conddb/time_arrow.png';

    if( ! $tagsCanvas.tagcanvas({
        textColour : '#ffffff',
        outlineThickness : 1,
        outlineOffset : 15,
        pulsateTo : 0.2,
        pulsateTime : 0.75,
        maxSpeed : 0.05,
        reverse : true,
        weight : true,
        weightMode : 'colour',
        weightFrom : 'data-weight',
        shape : 'hring(1.5)',
        offsetX: -300,
        zoom : 1.1,
        lock : 'x',
        centreFunc : drawTextAtCenter(ctx, detector_type, $tagsCanvas.width()/2., $tagsCanvas.height()/2.)
      })) {
        // TagCanvas failed to load
        $tagsCanvas.hide();
      };

    // Prepare the tags dialog
    var $dlg_cnt = $("#tagsCanvasContainer");
    $dlg_cnt.dialog({
        title: partition + " global tags of " + detector_type + " detector type",
        show: {effect:"fade", duration:800},
        hide: {effect:"fade", duration:1000},
        height: dialHeight,
        width: dialWidth,
        position:'right',
        autoOpen: false,
        resizable: true});

    // Fix the dialog when scrolling page
    $dlg_cnt.parent().css({position:"fixed"}).end().dialog('open');

    // Move dialogs to the right to open the page viewport space
    var $dtypesCanvas = $("#dtypesCanvas");
    $dlg_cnt.parent().animate({top: $dtypesCanvas.parent().offset().top - $(window).scrollTop() + 45});
    $dtypesCanvas.parent().animate({left:document.documentElement.clientWidth - $dtypesCanvas.parent().outerWidth()});

    // Remove overlay to be able to navigate through the page
    var overlayy = $(".ui-widget-overlay")[0];
    if (overlayy) {$('body')[0].removeChild(overlayy);};
};

// Draw text at the html5 canvas
function drawTextAtCenter(context, text, cx, cy) {
    context.font = '20pt bold sans-serif';
    context.textAlign = 'center';
    context.fillStyle = 'white';
    context.fillText(text, cx, cy);
};

// Get text description of a local tag
function getDescription(partition, local_tag){
  var partelem = $("span.tag:contains(" + local_tag + ")").filter(function(){
    return $(this).parent().find("span.partition").text() == partition;
  });
  return partelem.parents("li.note_header").children("ul.description").text();
};

function getAllDetectorTypes(partition, tag_class) {

    var detector_types = new Array();

    if (tag_class == 'globaltag') {
        $("ul > li > ul > li.summary > span.partition:contains(" + partition + ")")
        .each(function(i, elem){
            $('span.datatype', elem.parentNode.parentNode.parentNode)
            .each(function(){
                var text = this.textContent.replace('#','').replace(', ','');
                if (text != 'BK' && text != 'HLT') {
                    var pos = detector_types.indexOf(text);
                    if (pos == -1) { detector_types.push(text); };
                };
            });
        });
    }
    else if (tag_class == 'tag') {
        $("ul > li > ul > li:not(li.summary) > span.partition:contains(" + partition + ")")
        .each(function(i, elem){
            $('span.datatype', elem.parentNode)
            .each(function(){
                var text = this.textContent.replace('#','').replace(', ','');
                if (text != 'BK' && text != 'HLT') {
                    var pos = detector_types.indexOf(text);
                    if (pos == -1) { detector_types.push(text); };
                };
            });
        });
    };

    detector_types.sort(function(a,b){return b-a});
    detector_types.reverse();

    return detector_types;
};


function getAllTags(partition, detector_type, tag_class) {
    var tags = new Array();

    if (tag_class == 'globaltag') {
        $("li.summary > span.partition:contains(" + partition + ")").filter(function(){
            return $("span.datatype:contains(" + detector_type + ")", this.parentNode.parentNode.parentNode).length != 0;
        }).each(function(){
            var elem = $(this.parentNode.parentNode.parentNode);
            tags.push(elem.find("span.globaltag").text());
        });
    }
    else if (tag_class == 'tag') {
        $("li > span.partition:contains(" + partition + ")").filter(function(){
            return $("span.datatype:contains(" + detector_type + ")", this.parentNode).length != 0;
        }).each(function(){
            var elem = $(this.parentNode);
            tags.push(elem.find("span.tag").text());
        });
    };

    //tags.sort(function(a,b){return b-a});
    tags.reverse();
    return tags;
};

function scrollToTag(tag_name, partition_name, dest_type) {
    var destNote = $("span." + dest_type).filter(function(){
          var parent = $(this).parent();
          var foundPartition = parent.find("span.partition").text(); //concatenates all partitions if an entry is compound
          return ($(this).text() == tag_name && foundPartition.indexOf(partition_name) != -1);
        }).parents("li.note_header:first");
    var destination = destNote.find("span.contributor").offset().top;

    var frame = $.browser.webkit ? $('body:not(:animated)') : $('html:not(:animated)');
    frame.animate({scrollTop: destination-75}, 1000, function(){destNote.effect("highlight", {color: "#7B68EE"}, 1000);});
};

function scrollToTop(){
    $("html, body").animate({ scrollTop: 0 }, "slow");
};

#!/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
from subprocess import call

if os.environ.get('SQLDDDB_NO_UPDATE'):
    print('Skipping SQLDDDB downloading')
    exit(0)

SRC_URL = os.environ.get(
    'SQLDDDB_SRC_URL',
    'http://lhcb-rpm.web.cern.ch/lhcb-rpm/misc/SQLite-CondDB.tar.bz2')

siteroot = None
if 'LHCBRELEASES' in os.environ:
    siteroot = os.path.dirname(os.environ["LHCBRELEASES"].split(os.pathsep)[0])
else:
    siteroot = os.environ.get(
        'MYSITEROOT') or os.environ.get('RPM_INSTALL_PREFIX')
if not siteroot:
    raise RuntimeError(
        'cannot detect root of installation, please set $MYSITEROOT')

exit(call('curl -q -s -L {0} | tar -xjvf -'.format(SRC_URL),
          shell=True, cwd=os.path.join(siteroot, 'lhcb')))

###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# $Id: SQLDDDB-Oracle.py,v 1.3 2008-09-30 13:17:36 marcocle Exp $
__author__ = "Marco Clemencic <Marco.Clemencic@cern.ch>"

from Gaudi.Configuration import *
from DetCond.Configuration import *

##########################################################################
# Technology dependent options to use the Conditions database
##########################################################################
from Configurables import COOLConfSvc
COOLConfSvc(UseLFCReplicaSvc=True)

CondDBAccessSvc("DDDB", ConnectionString="CondDB/DDDB", CacheHighLevel=1700)

CondDBAccessSvc(
    "LHCBCOND", ConnectionString="CondDB/LHCBCOND", CacheHighLevel=200)

CondDBAccessSvc(
    "SIMCOND", ConnectionString="CondDB/SIMCOND", CacheHighLevel=200)

CondDBAccessSvc(
    "DQFLAGS",
    ConnectionString="CondDB/DQFLAGS",
    CacheLowLevel=5,
    CacheHighLevel=10)

CondDBAccessSvc(
    "ONLINE",
    ConnectionString="CondDBOnline/ONLINE",
)

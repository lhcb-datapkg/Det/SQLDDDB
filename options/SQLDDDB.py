###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# $Id: SQLDDDB.py,v 1.13 2009-12-03 12:31:49 ishapova Exp $
__author__ = "Marco Clemencic <Marco.Clemencic@cern.ch>"

from os.path import exists, join

from Gaudi.Configuration import *
from DetCond.Configuration import *

##########################################################################
# Technology dependent options to use the Conditions database
##########################################################################


def getOfflineConnStr(partition):
    """Function to perform smart resolution of the SQLite DB file needed for a global tag."""

    tag = CondDB().Tags.get(partition)
    pathByTag = join(os.environ['SQLITEDBPATH'],
                     '%s_%s.db' % (partition, tag)) if tag else None

    if pathByTag and exists(pathByTag):
        return "sqlite_file:$SQLITEDBPATH/%s_%s.db/%s" % (partition, tag,
                                                          partition)
    else:
        return "sqlite_file:$SQLITEDBPATH/%s.db/%s" % (partition, partition)


CondDBAccessSvc(
    "DDDB", ConnectionString=getOfflineConnStr('DDDB'), CacheHighLevel=1700)

CondDBAccessSvc(
    "LHCBCOND",
    ConnectionString=getOfflineConnStr('LHCBCOND'),
    CacheHighLevel=200)

CondDBAccessSvc(
    "CALIBOFF",
    ConnectionString=getOfflineConnStr('CALIBOFF'),
    CacheHighLevel=200)

CondDBAccessSvc(
    "SIMCOND",
    ConnectionString=getOfflineConnStr('SIMCOND'),
    CacheHighLevel=200)

CondDBAccessSvc(
    "DQFLAGS",
    ConnectionString=getOfflineConnStr('DQFLAGS'),
    CacheLowLevel=5,
    CacheHighLevel=10)

# The Online partition consists on monthly snapshots
if "configureOnlineSnapshots" in dir():
    # We have a version of DetCond recent enough to set up the list of monthly
    # online snapshots

    def connStrFunc(ym_tuple):
        dbpath = os.environ["SQLITEDBPATH"]
        if exists(join(dbpath, "ONLINE-%04d%02d.db" % ym_tuple)):
            return "sqlite_file:$SQLITEDBPATH/ONLINE-%04d%02d.db/ONLINE" % ym_tuple
        return "sqlite_file:$SQLITEDBPATH/ONLINE-%04d.db/ONLINE" % ym_tuple[0]

    def getLatestSnapshot():
        """Detects the latest ONLINE snapshot present in $SQLITEDBPATH."""
        import re
        exp = re.compile(r"^ONLINE-(\d{4})(\d{2})?\.db$")
        dbpath = os.environ["SQLITEDBPATH"]
        snapshots = [
            m.groups() for m in map(exp.match, os.listdir(dbpath)) if m
        ]
        if not snapshots:
            raise Exception, "Cannot find Online snapshots in '%s'" % dbpath
        year, month = max(snapshots)
        year = int(year)
        if month: month = int(month)
        return (year, month)

    ## This is to automatically select the previous month
    # latest_snapshot = None
    configureOnlineSnapshots(connStrFunc=connStrFunc, end=getLatestSnapshot())
